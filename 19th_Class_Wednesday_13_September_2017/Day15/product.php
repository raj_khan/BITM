<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li><a href="supplier.php">Supplier</a></li>
				<li class="active"><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Product Tab Section ********************-->
	<h3 class="product">Product</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label>Product Name</label>
			<input type="text" class="form-control" name="productName" placeholder="Please Enter Your Product Name..." required/>
			<label>Supplier ID</label>
			<input type="number" class="form-control" name="supplierID" placeholder="Please Enter Your SupplierID..." required/>
			<label>Customer ID</label>
			<input type="number" class="form-control" name="customerID" placeholder="Please Enter Your CustomerID..." required/>
			<label>Product Quantity</label>
			<input type="number" class="form-control" name="productQuantity" placeholder="Please Enter Your Product Quantity..." required/>
			<label>Product Unit Price</label>
			<input type="number" class="form-control" name="productUnitPrice" placeholder="Please Enter Your Product Unit Price..." required/><br>
			<button type="submit" class="btn btn-default" name="productSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["productSubmit"])){
						
					$productName = $_POST["productName"];						
					
					$supplierID = $_POST["supplierID"];
					
					$customerID =$_POST["customerID"];
					
					$productQuantity = $_POST["productQuantity"];
					
					$productUnitPrice = $_POST["productUnitPrice"];
					
					$sql = "INSERT INTO product(productName, supplierID, customerID, 	productQuantity, productUnitPrice) VALUES('$productName', '$supplierID', '$customerID', '$productQuantity', '$productUnitPrice')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM product ORDER BY productID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Product ID </strong> " . $row["productID"]. " <strong>  Product Name </strong>" . $row["productName"]. " <strong>  SupplierID   </strong>" . $row["supplierID"]. "<strong>   CustomerID   </strong>" . $row["customerID"].  " <strong>   product Quantity   	</strong>" . $row["productQuantity"]. "<strong>   Product Unit Price   </strong>" . $row["productUnitPrice"]. "<strong>   Product Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Product Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>