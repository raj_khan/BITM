<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li class="active"><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Supplier Tab Section ********************-->
	<h3 class="supplier">Supplier</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group" onsubmit="return check(supplierName)">
			<label>Supplier Name</label>
			<input type="text" class="form-control" name="supplierName" placeholder="Please Enter Your Supplier Name..." required/>
			<label>Supplier Number</label>
			<input type="number" class="form-control" name="supplierNumber" placeholder="Please Enter Your Supplier Number..." required/>
			<label>Supplier Address</label>
			<input type="text" class="form-control" name="supplierAddress" placeholder="Please Enter Your Supplier Address..." required/>
			<label>Supplier City</label>
			<input type="text" class="form-control" name="supplierCity" placeholder="Please Enter Your Supplier City..." required/>
			<label>Supplier Country</label>
			<input type="text" class="form-control" name="supplierCountry" placeholder="Please Enter Your Supplier Country..." required/><br>
			<button type="submit" class="btn btn-default"  onclick="check(supplierName)" name="supplierSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["supplierSubmit"])){
						
					$supplierName = $_POST["supplierName"];						
					
					$supplierNumber = $_POST["supplierNumber"];
					
					$supplierAddress =$_POST["supplierAddress"];
					
					$supplierCity = $_POST["supplierCity"];
					
					$supplierCountry = $_POST["supplierCountry"];
					
					$sql = "INSERT INTO supplier(supplierName, supplierNumber, supplierAddress, supplierCity, supplierCountry) VALUES('$supplierName', '$supplierNumber', '$supplierAddress', '$supplierCity', '$supplierCountry')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM supplier ORDER BY supplierID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Supplier ID </strong> " . $row["supplierID"]. " <strong>  Supplier Name </strong>" . $row["supplierName"]. " <strong>  Supplier Number   </strong>" . $row["supplierNumber"]. "<strong>   Supplier Address   </strong>" . $row["supplierAddress"].  " <strong>   Supplier City   	</strong>" . $row["supplierCity"]. "<strong>   Supplier Country   </strong>" . $row["supplierCountry"]. "<strong>   Supplier Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Supplier Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>
<!--*********** Start JavaScript *************-->
<script>
	function check(supplierName)
		{
			var a = /^[0-9A-Za-z]+$/;
			if(supplierName.value.match(a))
			{
				document.getElementById('supplierName').innerHTML=" <p style='color:red'>Please fill your Username Only Alphabet...</p> ";
			}
			else
			{
				alert("Please fill your Username Only Alphabet.....");
			}
		}
</script>
<!--*********** End JavaScript *************-->