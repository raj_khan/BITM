<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li class="active"><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li class="active"><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Employee Tab Section ********************-->
	<h3 class="employee">Employee</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label>Employee Name</label>
			<input type="text" class="form-control" name="employeeName" placeholder="Please Enter Your Employee Name..." required/>
			<label>Employee Number</label>
			<input type="number" class="form-control" name="employeeNumber" placeholder="Please Enter Your Employee Number..." required/>
			<label>Employee Address</label>
			<input type="text" class="form-control" name="employeeAddress" placeholder="Please Enter Your Employee Address..." required/>
			<label>Employee City</label>
			<input type="text" class="form-control" name="employeeCity" placeholder="Please Enter Your Employee City..." required/>
			<label>Employee Country</label>
			<input type="text" class="form-control" name="employeeCountry" placeholder="Please Enter Your Employee Country..." required/><br>
			<button type="submit" class="btn btn-default" name="employeeSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["employeeSubmit"])){
						
					$employeeName = $_POST["employeeName"];						
					
					$employeeNumber = $_POST["employeeNumber"];
					
					$employeeAddress =$_POST["employeeAddress"];
					
					$employeeCity = $_POST["employeeCity"];
					
					$employeeCountry = $_POST["employeeCountry"];
					
					$sql = "INSERT INTO employee(employeeName, employeeNumber, employeeAddress, employeeCity, employeeCountry) VALUES('$employeeName', '$employeeNumber', '$employeeAddress', '$employeeCity', '$employeeCountry')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM employee ORDER BY employeeID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Employee ID </strong> " . $row["employeeID"]. " <strong>  Employee Name </strong>" . $row["employeeName"]. " <strong>  Employee Number   </strong>" . $row["employeeNumber"]. "<strong>   Employee Address   </strong>" . $row["employeeAddress"].  " <strong>   Employee City   	</strong>" . $row["employeeCity"]. "<strong>   Employee Country   </strong>" . $row["employeeCountry"]. "<strong>   Employee Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Employee Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>