<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li class="active"><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li class="active"><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Transaction Tab Section ********************-->
	<h3 class="transaction">Transaction</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label>Customer ID</label>
			<input type="number" class="form-control" name="customerID" placeholder="Please Enter Your CustomerID..." required/>
			<label>Supplier ID</label>
			<input type="number" class="form-control" name="supplierID" placeholder="Please Enter Your SupplierID..." required/>
			<label>Product Category ID</label>
			<input type="number" class="form-control" name="productcategoryID" placeholder="Please Enter Your productcategoryID..." required/>
			<label>Product Details ID</label>
			<input type="number" class="form-control" name="productdetailsID" placeholder="Please Enter Your productdetailsID ..." required/>
			<label>Employee ID</label>
			<input type="number" class="form-control" name="employeeID" placeholder="Please Enter Your employeeID..." required/>
			<label>Product ID</label>
			<input type="number" class="form-control" name="productID" placeholder="Please Enter Your productID..." required/>
			<label>Transaction Type</label>
			<input type="text" class="form-control" name="transactionType" placeholder="Please Enter Your transactionType..." required/>
			<label>Total Amount</label>
			<input type="number" class="form-control" name="totalAmount" placeholder="Please Enter Your totalAmount..." required/>
			<label>Due Amount</label>
			<input type="number" class="form-control" name="dueAmount" placeholder="Please Enter Your dueAmount..." required/>
			<label>Transaction Status</label>
			<input type="text" class="form-control" name="transactionStatus" placeholder="Please Enter Your transactionStatus..." required/><br>
			<button type="submit" class="btn btn-default" name="transactionSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["transactionSubmit"])){
						
					$customerID = $_POST["customerID"];						
					
					$supplierID = $_POST["supplierID"];
					
					$productcategoryID =$_POST["productcategoryID"];
					
					$productdetailsID = $_POST["productdetailsID"];
					
					$employeeID = $_POST["employeeID"];
					
					$productID = $_POST["productID"];
					
					$transactionType = $_POST["transactionType"];
					
					$totalAmount = $_POST["totalAmount"];
					
					$dueAmount = $_POST["dueAmount"];
					
					$transactionStatus = $_POST["transactionStatus"];
					
					$sql = "INSERT INTO transactiontable(customerID, supplierID, productcategoryID, productdetailsID, employeeID, productID, transactionType, totalAmount, dueAmount, transactionStatus) VALUES('$customerID', '$supplierID', '$productcategoryID', '$productdetailsID', '$employeeID', '$productID', '$transactionType' ,'$totalAmount', '$dueAmount', '$transactionStatus')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM transactiontable ORDER BY transactionID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Transaction ID </strong> " . $row["transactionID"]. " <strong>   Customer ID </strong>" . $row["customerID"]. " <strong>  SupplierID   </strong>" . $row["supplierID"]. "<strong>   Product Category ID   </strong>" . $row["productcategoryID"].  " <strong>   product Details ID   	</strong>" . $row["productdetailsID"]. "<strong>   Employee ID  </strong>" . $row["employeeID"]."<strong>   Product ID</strong>".$row["productID"] ."<strong>  Transaction Type</strong>".$row["transactionType"]."<strong>   Total Amount</strong>".$row["totalAmount"]."<strong>   Due Amount</strong>".$row["dueAmount"]."<strong>   Transaction Status</strong>".$row["transactionStatus"]."<strong>   Product Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Transaction Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>