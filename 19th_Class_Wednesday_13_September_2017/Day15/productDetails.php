<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li class="active"><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Product Details Tab Section ********************-->
	<h3 class="productDetails">Product Details</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label>Product Details Name</label>
			<input type="text" class="form-control" name="productDetaiName" placeholder="Please Enter Your Product Details Name..." required/>
			<label>Product Details Size</label>
			<input type="text" class="form-control" name="productDetailsSize" placeholder="Please Enter Product Details Size..." required/>
			<label>Product Details Quantity</label>
			<input type="number" class="form-control" name="productDetailsQuantity" placeholder="Please Enter Product Details Quantity..." required/>
			<label>Product Details Per Unit Buying Price</label>
			<input type="number" class="form-control" name="productDetailsPerUnitBuyingPrice" placeholder="Please Enter Your Product Details Per Unit Buying Price..." required/>
			<label>Product Details Per Unit Selling Price</label>
			<input type="number" class="form-control" name="productDetailsPerUnitSellingPrice" placeholder="Please Enter Your Product Details Per Unit Selling Price..." required/>
			<label>Product Expires Date</label>
			<input type="date" class="form-control" name="expireDate" placeholder="Please Enter Your Product Expires Date..." required/><br>
			<button type="submit" class="btn btn-default" name="productDetailsSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["productDetailsSubmit"])){
						
					$productDetaiName = $_POST["productDetaiName"];						
					
					$productDetailsSize = $_POST["productDetailsSize"];
					
					$productDetailsQuantity =$_POST["productDetailsQuantity"];
					
					$productDetailsPerUnitBuyingPrice = $_POST["productDetailsPerUnitBuyingPrice"];
					
					$productDetailsPerUnitSellingPrice = $_POST["productDetailsPerUnitSellingPrice"];
					
					$expireDate = $_POST["expireDate"];
					
					$sql = "INSERT INTO productdetails(	productDetailsName, productDetailsSize, productDetailsQuantity, 	productDetailsPerUnitBuyingPrice, productDetailsPerUnitSellingPrice, expireDate) VALUES('$productDetaiName', '$productDetailsSize', '$productDetailsQuantity', '$productDetailsPerUnitBuyingPrice', '$productDetailsPerUnitSellingPrice', '$expireDate')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM productdetails ORDER BY productDetailsID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Product Details ID </strong> " . $row["productDetailsID"]. " <strong>  Product Details Name </strong>" . $row["productDetailsName"]. " <strong>  Product Details Size   </strong>" . $row["productDetailsSize"]. "<strong>   Product Details Quantity   </strong>" . $row["productDetailsQuantity"]. " <strong>   Product Details Per Unit Buying Price  	</strong>" . $row["productDetailsPerUnitBuyingPrice"]. "<strong>   Product Details Per Unit Selling Price   </strong>" . $row["productDetailsPerUnitSellingPrice"]. "<strong>Product Expires Date</strong>" .$row["expireDate"]   . "<strong>   Product Details Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Product Details Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>