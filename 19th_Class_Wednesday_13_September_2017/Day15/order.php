<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li><a href="customer.php">Customer</a></li>
				<li><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li class="active"><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Order Tab Section ********************-->
	<h3 class="order">Order</h3>
	<form method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label>Customer ID</label>
			<input type="number" class="form-control" name="customerID" placeholder="Please Enter Your CustomerID..." required/>
			<label>Order Quantity</label>
			<input type="number" class="form-control" name="orderQuantity" placeholder="Please Enter Your OrderQuantity..." required/>
			<label>Employee ID</label>
			<input type="number" class="form-control" name="employeeID" placeholder="Please Enter Your EmployeeID..." required/>
			<label>Order Date</label>
			<input type="date" class="form-control" name="orderDate" placeholder="Please Enter Your Product Order Date..." required/><br>
			<button type="submit" class="btn btn-default" name="ordertableSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["ordertableSubmit"])){
						
					$customerID = $_POST["customerID"];						
					
					$orderQuantity = $_POST["orderQuantity"];
					
					$employeeID =$_POST["employeeID"];
					
					$orderDate = $_POST["orderDate"];
					
					$sql = "INSERT INTO ordertable(customerID, orderQuantity, employeeID, 	orderDate) VALUES('$customerID', '$orderQuantity', '$employeeID', '$orderDate')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM ordertable ORDER BY orderID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Order ID </strong> " . $row["orderID"]. " <strong>  Customer ID </strong>" . $row["customerID"]. " <strong>  Order Quantity   </strong>" . $row["orderQuantity"]. "<strong>   Employee ID   </strong>" . $row["employeeID"].  " <strong>   Order Date    	</strong>" . $row["orderDate"]. "<strong>   Product Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
	<!--******************** End Order Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>