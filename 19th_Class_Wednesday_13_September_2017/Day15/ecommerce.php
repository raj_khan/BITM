<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ecommerce</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container">
	  <div class="jumbotron">
		<h1 class="text-center">Ecommerce</h1>
		<ul class="nav nav-tabs">
			<li><a data-toggle="tab" href="#home"><span class="glyphicon glyphicon-home"></span></a></li>
			<li><a data-toggle="tab" href="#customer">Customer</a></li>
			<li><a data-toggle="tab" href="#supplier">Supplier</a></li>
			<li><a data-toggle="tab" href="#product">Product</a></li>
			<li><a data-toggle="tab" href="#productCategory">Product Category</a></li>
			<li><a data-toggle="tab" href="#productDetails">Product Details</a></li>
			<li><a data-toggle="tab" href="#employee">Employee</a></li>
			<li><a data-toggle="tab" href="#transaction">Transaction</a></li>
	    </ul>
	  </div>
	  <!--******************** Start Main Tab Content Section ********************-->
	  <div class="tab-content">
		<!--******************** Start Home Tab Section ********************-->
		<div id="home" class="tab-pane fade in active">
		  <h3>HOME</h3>
		  <p>Started his hearted a</p>
		</div>		
		<!--******************** End Home Tab Section ********************-->
		
		<!--******************** Start Customer Tab Section ********************-->
		<div id="customer" class="tab-pane fade">
		  <h3>Customer</h3>
		  <form method="post" enctype="multipart/form-data">
			<div class="form-group" onsubmit="return check(customerName)">
				<label for="customerName">Customer Name</label>
				<input type="text" class="form-control" id="customerName" name="customerName" placeholder="Please Enter Your Customer Name..." required>
				<label for="customerNumber">Customer Number</label>
				<input type="number" class="form-control" id="customerNumber" name="customerNumber" placeholder="Please Enter Your Customer Number..." required>
				<label for="customerAddress">Customer Address</label>
				<input type="text" class="form-control" id="customerAddress" name="customerAddress" placeholder="Please Enter Your Customer Address..." required>
				<label for="customerCity">Customer City</label>
				<input type="text" class="form-control" id="customerCity" name="customerCity" placeholder="Please Enter Your Customer City..." required>
				<label for="customerCountry">Customer Country</label>
				<input type="text" class="form-control" id="customerCountry" name="customerCountry" placeholder="Please Enter Your Customer Country..." required><br>
				<button type="submit" class="btn btn-default"  onclick="check(customerName)" name="customerSubmit">Submit</button>
				<input type="reset" class="btn btn-default" value="Reset"/>
			</div>
				<!--*********** Start Php *************-->
				<?php
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "ecommerce";
				
				error_reporting(0);
				
				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);

				// Check connection
				if ($conn->connect_error) {
					die("<p style='color:red'>Database Connection failed...!!</p>" . $conn->connect_error);
				} 
				echo "<p style='color:#00b300'>Database Connected successfully...!!</p>";
				
					if(isset($_POST["customerSubmit"])){
							
						$customerName = $_POST["customerName"];						
						
						$customerNumber = $_POST["customerNumber"];
						
						$customerAddress =$_POST["customerAddress"];
						
						$customerCity = $_POST["customerCity"];
						
						$customerCountry = $_POST["customerCountry"];
						
						$sql = "INSERT INTO customer(customerName, customerNumber, customerAddress, customerCity, customerCountry) VALUES('$customerName', '$customerNumber', '$customerAddress', '$customerCity', '$customerCountry')";
						
						
						if ($conn->query($sql) === TRUE) {
							echo "<p style='color:#0000ff'>New record created successfully</p>";
						} else {
							echo "Error: " . $sql . "<br>" . $conn->error;
						}	
						
						$sql = "SELECT * FROM customer ORDER BY customerID DESC LIMIT 1";
						$result = $conn->query($sql);

						if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
								echo " <strong>New Customer ID </strong> " . $row["customerID"]. " <strong>  Customer Name </strong>" . $row["customerName"]. " <strong>  Customer Number   </strong>" . $row["customerNumber"]. "<strong>   Customer Address   </strong>" . $row["customerAddress"].  " <strong>   Customer City   	</strong>" . $row["customerCity"]. "<strong>   Customer Country   </strong>" . $row["customerCountry"]. "<strong>   Customer Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted</strong>  <br>";
							}
						} else {
							echo "0 results";
						}				
					}		
				$conn->close();
				?>
				<!--*********** End Php *************-->				
		  </form>
		</div>
		<!--******************** End Customer Tab Section ********************-->
		
		<!--******************** Start Supplier Tab Section ********************-->
		<div id="supplier" class="tab-pane fade">
		  <h3>Supplier</h3>
		  <form method="post" enctype="multipart/form-data">
			<div class="form-group" onsubmit="return check(customerName)">
				<label for="customerName">Customer Name</label>
				<input type="text" class="form-control" id="customerName" name="customerName" placeholder="Please Enter Your Customer Name..." required>
				<label for="customerNumber">Customer Number</label>
				<input type="number" class="form-control" id="customerNumber" name="customerNumber" placeholder="Please Enter Your Customer Number..." required>
				<label for="customerAddress">Customer Address</label>
				<input type="text" class="form-control" id="customerAddress" name="customerAddress" placeholder="Please Enter Your Customer Address..." required>
				<label for="customerCity">Customer City</label>
				<input type="text" class="form-control" id="customerCity" name="customerCity" placeholder="Please Enter Your Customer City..." required>
				<label for="customerCountry">Customer Country</label>
				<input type="text" class="form-control" id="customerCountry" name="customerCountry" placeholder="Please Enter Your Customer Country..." required><br>
				<button type="submit" class="btn btn-default"  onclick="check(customerName)" name="customerSubmit">Submit</button>
				<input type="reset" class="btn btn-default" value="Reset"/>
			</div>
				<!--*********** Start Php *************-->
				<?php
				$servername = "localhost";
				$username = "root";
				$password = "";
				$dbname = "ecommerce";
				
				error_reporting(0);
				
				// Create connection
				$conn = new mysqli($servername, $username, $password, $dbname);

				// Check connection
				if ($conn->connect_error) {
					die("<p style='color:red'>Database Connection failed...!!</p>" . $conn->connect_error);
				} 
				echo "<p style='color:#00b300'>Database Connected successfully...!!</p>";
				
					if(isset($_POST["customerSubmit"])){
							
						$customerName = $_POST["customerName"];						
						
						$customerNumber = $_POST["customerNumber"];
						
						$customerAddress =$_POST["customerAddress"];
						
						$customerCity = $_POST["customerCity"];
						
						$customerCountry = $_POST["customerCountry"];
						
						$sql = "INSERT INTO customer(customerName, customerNumber, customerAddress, customerCity, customerCountry) VALUES('$customerName', '$customerNumber', '$customerAddress', '$customerCity', '$customerCountry')";
						
						
						if ($conn->query($sql) === TRUE) {
							echo "<p style='color:#0000ff'>New record created successfully</p>";
						} else {
							echo "Error: " . $sql . "<br>" . $conn->error;
						}	
						
						$sql = "SELECT * FROM customer ORDER BY customerID DESC LIMIT 1";
						$result = $conn->query($sql);

						if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
								echo " <strong>New Customer ID </strong> " . $row["customerID"]. " <strong>  Customer Name </strong>" . $row["customerName"]. " <strong>  Customer Number   </strong>" . $row["customerNumber"]. "<strong>   Customer Address   </strong>" . $row["customerAddress"].  " <strong>   Customer City   	</strong>" . $row["customerCity"]. "<strong>   Customer Country   </strong>" . $row["customerCountry"]. "<strong>   Customer Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted</strong>  <br>";
							}
						} else {
							echo "0 results";
						}				
					}		
				$conn->close();
				?>
				<!--*********** End Php *************-->				
		  </form>
		</div>
		<!--******************** End Supplier Tab Section ********************-->
		
		<!--******************** Start Product Tab Section ********************-->
		<div id="product" class="tab-pane fade">
		  <h3>Product</h3>
		  <p>JavaScript cript was influenced by programming languages such as Self and Scheme.[7]</p>
		</div>
		<!--******************** End Product Tab Section ********************-->
		
		<!--******************** Start Product Category Tab Section ********************-->
		<div id="productCategory" class="tab-pane fade">
		  <h3>Product Category</h3>
		  <p>JavaScript cript was influenced by programming languages such as Self and Scheme.[7]</p>
		</div> 
		<!--******************** End Product Category Tab Section ********************-->
		
		<!--******************** Start Product Details Tab Section ********************-->
		<div id="productDetails" class="tab-pane fade">
		  <h3>Product Details</h3>
		  <p>JavaScript cript was influenced by programming languages such as Self and Scheme.[7]</p>
		</div>
		<!--********************  End Product Details Tab Section ********************-->

		<!--******************** Start Employee Tab Section ********************-->
		<div id="employee" class="tab-pane fade">
		  <h3>Employee</h3>
		  <p>JavaScript cript was influenced by programming languages such as Self and Scheme.[7]</p>
		</div>  
		<!--******************** End Employee Tab Section ********************-->
		
		<!--******************** Start Transaction Tab Section ********************-->
		<div id="transaction" class="tab-pane fade">
		  <h3>Transaction</h3>
		  <p>JavaScript cript was influenced by programming languages such as Self and Scheme.[7]</p>
		</div> 
		<!--******************** End Transaction Tab Section ********************-->
		
	  </div>	  
	  <!--******************** End Main Tab Content Section ********************-->
	</div>	
</body>
</html>

<!--*********** Start JavaScript *************-->
<script>
	function check(customerName)
		{
			var a = /^[0-9A-Za-z]+$/;
			if(customerName.value.match(a))
			{
				//alert("Success");
				//document.write("Success");
				//document.getElementById("demo");
				document.getElementById('customerName').innerHTML=" <p style='color:red'>Please fill your Username Only Alphabet...</p> ";
			}
			else
			{
				alert("Please fill your Username Only Alphabet.....");
				//document.getElementById('customerName').innerHTML=" <p style='color:red'>Please fill your Username Only Alphabet...</p> ";
			}
		}
</script>
<!--*********** End JavaScript *************-->