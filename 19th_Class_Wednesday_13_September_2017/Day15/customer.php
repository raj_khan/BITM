<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
</head>
<body>
<!--******************** Start Container Section ********************-->
<div class="container">
	<!--******************** Start Header Section ********************-->
	<div class="tab-content">
		<div class="jumbotron">
			<h1 class="text-center">Ecommerce</h1>
			<ul class="nav nav-tabs">
				<li><a href="home.php"><span class="glyphicon glyphicon-home"></span></a></li>
				<li class="active"><a href="customer.php">Customer</a></li>
				<li><a href="supplier.php">Supplier</a></li>
				<li><a href="product.php">Product</a></li>
				<li><a href="productCategory.php">Product Category</a></li>
				<li><a href="productDetails.php">Product Details</a></li>
				<li><a href="order.php">Order</a></li>
				<li><a href="employee.php">Employee</a></li>
				<li><a href="transaction.php">Transaction</a></li>
			</ul>
		</div>		
	</div>
	<!--******************** End Header Section ********************-->	
	
	<!--******************** Start Customer Tab Section ********************-->
	<h3 class="customer">Customer</h3>			  
	  <form method="post" enctype="multipart/form-data">
		<div class="form-group" onsubmit="return check(customerName)">
			<label>Customer Name</label>
			<input type="text" class="form-control" name="customerName" placeholder="Please Enter Your Customer Name..." required/>
			<label>Customer Number</label>
			<input type="number" class="form-control" name="customerNumber" placeholder="Please Enter Your Customer Number..." required/>
			<label>Customer Address</label>
			<input type="text" class="form-control" name="customerAddress" placeholder="Please Enter Your Customer Address..." required/>
			<label>Customer City</label>
			<input type="text" class="form-control" name="customerCity" placeholder="Please Enter Your Customer City..." required/>
			<label>Customer Country</label>
			<input type="text" class="form-control" name="customerCountry" placeholder="Please Enter Your Customer Country..." required/><br>
			<button type="submit" class="btn btn-default"  onclick="check(customerName)" name="customerSubmit">Submit</button>
			<input type="reset" class="btn btn-default" value="Reset"/>
		</div>
			<!--*********** Start Php *************-->						
			<?php
			error_reporting(0);
			include'db_conn.php';
			
				if(isset($_POST["customerSubmit"])){
						
					$customerName = $_POST["customerName"];						
					
					$customerNumber = $_POST["customerNumber"];
					
					$customerAddress =$_POST["customerAddress"];
					
					$customerCity = $_POST["customerCity"];
					
					$customerCountry = $_POST["customerCountry"];
					
					$sql = "INSERT INTO customer(customerName, customerNumber, customerAddress, customerCity, customerCountry) VALUES('$customerName', '$customerNumber', '$customerAddress', '$customerCity', '$customerCountry')";
					
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:#0000ff'>New record created successfully</p>";
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}	
					
					$sql = "SELECT * FROM customer ORDER BY customerID DESC LIMIT 1";
					$result = $conn->query($sql);

					if ($result->num_rows > 0) {
						// output data of each row
						while($row = $result->fetch_assoc()) {
							echo " <strong>New Customer ID </strong> " . $row["customerID"]. " <strong>  Customer Name </strong>" . $row["customerName"]. " <strong>  Customer Number   </strong>" . $row["customerNumber"]. "<strong>   Customer Address   </strong>" . $row["customerAddress"].  " <strong>   Customer City   	</strong>" . $row["customerCity"]. "<strong>   Customer Country   </strong>" . $row["customerCountry"]. "<strong>   Customer Registration Date & Time   </strong>" . $row["reg_date"]."  <strong>     Inserted Successfully...!!</strong>  <br>";
						}
					} else {
						echo "0 results";
					}				
				}		
			$conn->close();
			?>
			<!--*********** End Php *************-->				
	  </form>
		
	<!--******************** End Customer Tab Section ********************-->
</div>
<!--******************** End Container Section ********************-->
</body>
</html>
<!--*********** Start JavaScript *************-->
<script>
	function check(customerName)
		{
			var a = /^[0-9A-Za-z]+$/;
			if(customerName.value.match(a))
			{				
				document.getElementById('customerName').innerHTML=" <p style='color:red'>Please fill your Username Only Alphabet...</p> ";
			}
			else
			{
				alert("Please fill your Username Only Alphabet.....");				
			}
		}
</script>
<!--*********** End JavaScript *************-->