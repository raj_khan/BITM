<?php
session_start();
if (isset($_SESSION['user'])) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <style>
                div.container {
                    width: 100%;
                    border: 1px solid gray;
                }

                header, footer {
                    padding: 1em;
                    color: white;
                    background-color: #3B5998;
                    clear: left;
                    text-align: center;
                }

                nav {
                    float: left;
                    max-width: 160px;
                    margin: 0;
                    padding: 1em;
                }

                nav ul {
                    list-style-type: none;
                    padding: 0;
                }

                nav ul a {
                    text-decoration: none;
                }

                article {
                    margin-left: 170px;
                    border-left: 1px solid gray;
                    padding: 1em;
                    overflow: hidden;
                }
            </style>
        </head>
        <body>

            <div class="container">

                <header>
                    <h1>My Point of Sales Admin Panel</h1>
                </header>

                <nav>
                    <ul>
                        <li><a href="#">Suplier</a></li>
                        <li><a href="#">Customer</a></li>
                        <li><a href="#">Item</a></li>
                    </ul>

                    <ul>
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </nav>

                <article>
                    <h1>Suplier</h1>
                    <table width="579" border="1">
                        <tbody>
                            <tr>
                                <td width="41">s/n</td>
                                <td width="75">name</td>
                                <td width="85">address</td>
                                <td width="91">contact</td>
                                <td width="108">email</td>
                                <td width="139">action</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>Details </p>
                    <h1>Item</h1>
                    <table width="579" border="1">
                        <tbody>
                            <tr>
                                <td width="41">s/n</td>
                                <td width="75">Item name</td>
                                <td width="85">qun</td>
                                <td width="91">.......</td>
                                <td width="108">.......</td>
                                <td width="139">action</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>Details </p>
                    <p>&nbsp;</p>
                </article>

                <footer>Copyright &copy; bitm.org.bd</footer>

            </div>

        </body>
    </html>
    <?php
} else {
    echo "<script>alert('Check Your Login Details ')</script>";
    echo "<script>window.location='index.php'</script>";
}
?>