<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include("config/config.php");


include("inc/header.php");
?>
<?php
include('inc/menu.php');


if (isset($_POST['passChangeSubmit'])) {

    $currentPass = $_POST['currentPass'];
    $newPass = $_POST['newPass'];
    $newConfirmPass = $_POST['newConfirmPass'];

    $userId = $_SESSION["s_id"];
    $query = "SELECT * FROM `admin` WHERE `id` = '$userId'";

    $checkUser = mysqli_query($con, $query);
    $userRow = mysqli_fetch_array($checkUser, MYSQLI_ASSOC);

    if (strcmp($userRow['pass'], $currentPass) == 0) {
        if (strcmp($userRow['pass'], $currentPass) == 0) {
            $updatePassQuery = "UPDATE `admin` SET `pass` = '$newPass' WHERE `admin`.`id` = $userId;";
            $updatedPassword = mysqli_query($con, $updatePassQuery);
            if ($updatedPassword) {
                $notification = "Your password is succesfully changned";
            } else {
                $notification = "Sorry your password is not changed";
            }
        } else {
            $notification = "Sorry Password and Confirm Password doesn't match";
        }
    } else {
        $notification = "Sorry your old password doesn't match";
    }
}
?>

<div class="content">
    <!--Detached content--> 
    <div class="container-detached">

        <!-- Grid -->
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <?php 
                if(isset($notification)){
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-styled-left">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                            <p class="text-center"><?php echo $notification; ?></p>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 style="padding:0 5px;" class="bg-slate-700 panel-title">Please provide current password and new password</h5>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">  
                            <div class="form-group" id="add_latest_title">
                                <label class="control-label col-lg-2">Current Password</label>
                                <div class="col-lg-10">
                                    <input type="password" placeholder="Please provide current password" name="currentPass" class="form-control">
                                </div>
                            </div>
                            <div class="form-group" id="add_latest_title">
                                <label class="control-label col-lg-2">New Passowrd</label>
                                <div class="col-lg-10">
                                    <input type="password" placeholder="Please provide New password"  name="newPass" class="form-control">
                                </div>
                            </div>
                            <div class="form-group" id="add_latest_title">
                                <label class="control-label col-lg-2">confirm Passowrd</label>
                                <div class="col-lg-10">
                                    <input type="password" placeholder="Please provide repeat password for confirm"  name="newConfirmPass" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="text-center">
                                    <input name="passChangeSubmit" value="Change Password" class="btn bg-slate" type="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="text-muted">
                <p class="text-center">&copy; 2017. Shyma- All Right Reserved. Designed by <a href="http://bsdbd.com/" target="_blank">BSD</a></p>
            </div>
        </div>
    </div>
    <!-- Footer -->

    <!-- /footer -->

</div>
<!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->

<script type="text/javascript">

    $(document).ready(function () {

        $('select#imageField').change(function () {
            var optionSelected = $(this).find("option:selected");
            var valueSelected = optionSelected.val();
            if (valueSelected == 'ms') {
                $(this).parents('.form-horizontal').find('#add_latest_title').hide();
                var inputField = $(this).parents('.form-horizontal').find("#add_latest_title input[name='add_latest_lift_title']");
                inputField.val('');
            } else {
                $(this).parents('.form-horizontal').find('#add_latest_title').show();
            }
        });

    });


//    $('#add_latest_title').hide();
</script>

</body>
</html>
