<?php
session_start();
if (isset($_SESSION["s_id"])) {
    header("location:../index.php");
}

include '../config/config.php';

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$error = NULL;

if (isset($_POST['login'])) {
    $email = test_input($_POST['userName']);
    $password = test_input($_POST['password']);

    $error = "";

    if ($email == "") {
        $error = "Enter Login Email";
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = "Enter Valid Mail";
    } else if ($password == "") {
        $error = "Enter Your Password";
    }


    if ($error == "") {

        $checkQuery = "SELECT * FROM admin where email='$email' and pass ='$password' and status='1'";

        $login_check_query = mysqli_query($con, $checkQuery);
        $login_check = mysqli_fetch_array($login_check_query, MYSQLI_ASSOC);


        if (strcmp($login_check['pass'], $password) == 0) {

            $_SESSION['s_id'] = $login_check['id'];
            header("location:../index.php");
        } else {
            $error = "Username or Password Does not Match";
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Customer Registration Form</title>

        <!-- Bootstrap -->
        <link href="../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="../asset/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>



        <!-- Content area -->
        <div class="content" >
            <?php if (isset($error) && !empty($error)) { ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="alert alert-warning alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                <span class="text-semibold"> <?php echo $error ?> </span>
                            </div>
                        </div>
                    </div>
                </div> 
            <?php } ?>







            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">
                                <span class="glyphicon glyphicon-bookmark"></span> Welcome To Raj Dashboard
                            </h3>
                        </div>

                        <div class="container">
                            <div class="col-md-4 col-md-offset-4">
                                <section>
                                    <div class="panel panel-default top caja">
                                        <div class="panel-body">
                                            <h3 class="text-center">Admin Login</h3>

                                            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                    <input type="email" name="userName" id="email" class="form-control" placeholder="Email" aria-describedby="sizing-addon1" required>
                                                </div>
                                                <br>
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-key" aria-hidden="true"></i></span>
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" aria-describedby="sizing-addon1" required>
                                                </div>
                                                <br>
                                                <button type="submit" name="login" class="btn btn-primary btn-block">Ingresar</button>

                                            </form>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>

                        <div class="panel-body">


                            <a style="margin-top: 300px;" href="" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Raj Khan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../js/main.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../asset/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('select[name = "prod_catogory"]').select2();
            });
        </script>

    </body>
</html>
