<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include 'config/config.php';

include '../admin/inc/header.php';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <span class="glyphicon glyphicon-bookmark"></span> Welcome To Raj Dashboard
                    </h3>
                </div>
                <div class="panel-body">
<?php include '../admin/inc/menu.php' ?>


                    <a style="margin-top: 300px;" href="" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Raj Khan</a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
include '../admin/inc/footer.php';
?>