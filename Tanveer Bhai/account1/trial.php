<?php 
include("config.php"); 
include("include/header.php");

?>
<h1>Trial Balance</h1>
<form action="" method="get">
	from: 
	<input type="date" name="from"/>
	to: 
	<input type="date" name="to"/>
	<input type="submit" name="submit" value="submit"/>
</form>
<br/>

<?php 
	if( isset($_GET['submit']) && !empty($_GET['submit'])  ) {
?>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>COA ID</th>
		<th>particulars</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
<?php
	extract($_GET);
	
	$accounts = $db->select("SELECT * FROM `accounts`");
	$totalDr = 0;
	$totalCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("
			SELECT sum(`journal_view`.`amount`) as dr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'
			
		");
		
		$ledgerCr = $db->select("
			SELECT SUM(`journal_view`.`amount`) as cr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'
		");
		$amount = $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalDr = $totalDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalCr = $totalCr + $cr;
		}
		if($amount !== 0)
		{
?>
	<tr>
		<td><?php echo $account['id']; ?></td>
		<td><?php echo $account['name']; ?></td>
		<td><?php echo $dr; ?></td>
		<td><?php echo $cr; ?></td>
	</tr>
<?php
		} // if amount not zero
	}	// end foreach
?>

	<tr>
		<th colspan="2" > Total </th>
		<th><?php echo $totalDr; ?></th>
		<th><?php echo $totalCr; ?></th>
	</tr>
	
</table>
<?php
}  // end get 
?>


<?php include("include/footer.php"); ?>