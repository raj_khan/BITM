<?php include("config.php"); ?>
<?php include("include/header.php"); ?>
<h1>Ledger</h1>
<?php 

$ledgerDr = $db->select("SELECT `vouchers`.`trans_id`, `vouchers`.`account_id`, `accounts`.`name`, SUM(`vouchers`.`amount`) as debit FROM `accounts` LEFT JOIN `vouchers` ON `accounts`.`id` = `vouchers`.`account_id` WHERE `vouchers`.`type` = 'dr' GROUP BY `vouchers`.`account_id`");

$ledgerCr = $db->select("SELECT `vouchers`.`trans_id`, `vouchers`.`account_id`, `accounts`.`name`, SUM(`vouchers`.`amount`) as debit FROM `accounts` LEFT JOIN `vouchers` ON `accounts`.`id` = `vouchers`.`account_id` WHERE `vouchers`.`type` = 'cr' GROUP BY `vouchers`.`account_id`");
//print_r($ledgerDr);

$accounts = $db->select("SELECT * FROM `accounts`");

foreach($accounts as $account){ 
	$accountId = $account['id'];
	$ledgers = $db->select("SELECT `transection`.`date`, `accounts`.`name`, `vouchers`.`account_id` as ref, `vouchers`.`type`, `vouchers`.`amount` FROM `accounts` LEFT JOIN `vouchers` ON `accounts`.`id` = `vouchers`.`ladger` LEFT JOIN `transection` ON `vouchers`.`trans_id` = `transection`.`id` WHERE `vouchers`.`account_id` = $accountId ORDER BY `transection`.`date`, `vouchers`.`type` DESC");
	
?>
<h4><?php echo $account['name']; ?></h4>
<table border='1' >
	<tr>
		<th>date</th>
		<th>particulars</th>
		<th>COA ID</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
	<?php foreach($ledgers as $ledger) { ?>
	<tr>
		<td><?php echo $ledger['date']; ?></td>
		<td><?php echo $ledger['name']; ?></td>
		<td><?php echo $ledger['ref']; ?></td>
		<td><?php echo ($ledger['type'] == 'dr') ? $ledger['amount'] : ''; ?></td>
		<td><?php echo ($ledger['type'] == 'cr') ? $ledger['amount'] : ''; ?></td>
	</tr>
	<?php } ?>
	<tr>
		<th colspan="3" > Total</th>
		<th></th>
		<th></th>
	
	</tr>
	
</table>
<?php } ?>


<?php include("include/footer.php"); ?>