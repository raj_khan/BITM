<?php
	
	
class Database
{
	private $connect;
	
	public $host   = DB_HOST;
	public $user   = DB_USER;
	public $pass   = DB_PASS;
	public $dbname = DB_NAME;
	
	public function __construct()
	{
		$this->connect = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname.';charset=utf8', $this->user, $this->pass);
		$this->connect->setAttribute(PDO::ATTR_ERRMODE, "PDO::ERRMODE_EXCEPTION"); 
		$this->connect->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}
	
	public function select($query)
	{
		$sql = $this->connect->query($query);
		$data = $sql->fetchAll(PDO::FETCH_ASSOC);
		if($sql){
			return $data;
		} else {
			return false;
		}
	}
	
	public function insert($query)
	{
		$sql = $this->connect->query($query);
        $lastInsertId = $this->connect->lastInsertId();
		
		if($sql){
			return $lastInsertId;
		} else {
			return false;
		}
	}
	
	public function update($query)
	{
		$sql = $this->connect->query($query);
		$result = $sql->rowCount();

		if($sql){
			return $result;
		} else {
			return false;
		}
	}
	
	public function delete($query)
	{
		$sql = $this->connect->query($query);
		$result = $sql->rowCount();
		if($sql){
			return $result;
		} else {
			return false;
		}
	}
	
}
	
	
	
	

?>