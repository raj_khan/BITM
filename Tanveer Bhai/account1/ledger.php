<?php 
include("config.php"); 
include("include/header.php");

$accountsAll = $db->select("SELECT * FROM `accounts`");

if( isset($_GET['account']) && !empty($_GET['account'])  ) {
	extract($_GET);
	$accounts = $db->select("SELECT name FROM `accounts` where id = $account ");
	$ledgers = $db->select("SELECT `journal`.`trans_date`, `accounts`.`name`, `journal_view`.`type`,`journal_view`.`amount` , `journal_view`.`bd`, `journal_view`.`cd` FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` LEFT JOIN `accounts` ON `accounts`.`id` = `journal_view`.`ladger` WHERE `journal_view`.`account_id` = $account ORDER BY `journal`.`trans_date`");
	
	$balanceBd = $db->select("SELECT `journal_view`.`bd` FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE `journal_view`.`account_id` = $account ORDER BY `journal`.`trans_date`LIMIT 1");
	
	
}

?>
<h1>Ledger</h1>
<form action="" method="get">
	select Accounts: 
	<select name="account">
	<?php foreach($accountsAll as $account){ ?>
		<option value="<?php echo $account['id']; ?>"><?php echo $account['name']; ?></option>
	<?php } ?>
	</select>
	<input type="submit" value="submit"/>
</form>
<br/>

<?php if(@$ledgers){ ?>
<h3><?php echo @$accounts[0]['name']; ?></h3>

<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>date</th>
		<th>particulars</th>
		<th>Dr.</th>
		<th>Cr.</th>
		<th>Balance</th>
	</tr>
	
	<tr>
		<td></td>
		<td>Balance b/d</td>
		<td><?php echo $balanceBd[0]['bd'];?></td>
		<td></td>
	</tr>
	
	<?php 
	$balanceCd = $balanceBd[0]['bd']; 
	foreach(@$ledgers as $ledger) { 
	
	($ledger['type'] == 'dr') ? $balanceCd = $balanceCd + $ledger['amount'] : $balanceCd = $balanceCd - $ledger['amount'] ;
	
	?>
	<tr>
		<td><?php echo $ledger['trans_date']; ?></td>
		<td><?php echo $ledger['name']; ?></td>
		<td><?php echo ($ledger['type'] == 'dr') ? $ledger['amount'] : ''; ?></td>
		<td><?php echo ($ledger['type'] == 'cr') ? $ledger['amount'] : ''; ?></td>
		<td><?php echo $ledger['cd']; ?></td>
	</tr>
	<?php } ?>
	
	<tr>
		<th colspan="2" > balance c/d</th>
		<th><?php echo $balanceCd; ?></th>
		<th></th>
	</tr>
	
</table>
<?php } ?>


<?php include("include/footer.php"); ?>