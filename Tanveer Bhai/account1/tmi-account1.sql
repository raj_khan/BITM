-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2017 at 05:53 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tmi-account1`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `code` double NOT NULL,
  `name` varchar(155) NOT NULL,
  `account_type_id` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `cf` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `code`, `name`, `account_type_id`, `type`, `cf`) VALUES
(7, 101, 'cash', 3, 'dr', 440000),
(8, 103, 'capital', 5, 'cr', -1000000),
(9, 102, 'advance office rent', 3, 'dr', 200000),
(10, 104, 'furnitures', 3, 'dr', 200000),
(11, 105, 'purchases', 1, 'dr', 400000),
(12, 106, 'sales', 2, 'cr', -600000),
(13, 107, 'accounts receivable', 3, 'dr', 360000),
(14, 108, 'salary', 1, 'dr', 0),
(15, 109, 'office rent', 1, 'dr', 0),
(16, 110, 'utility bill', 1, 'dr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE `account_type` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `name`) VALUES
(1, 'expenditure'),
(2, 'income'),
(3, 'assets'),
(4, 'liabilities'),
(5, 'owner\'s equity');

-- --------------------------------------------------------

--
-- Table structure for table `journal`
--

CREATE TABLE `journal` (
  `id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `j_type` varchar(100) NOT NULL,
  `details` varchar(255) NOT NULL,
  `naration` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal`
--

INSERT INTO `journal` (`id`, `trans_id`, `trans_date`, `j_type`, `details`, `naration`, `create_at`) VALUES
(1, 1, '2017-09-01', 'general', 'capital investment in cash', 'capital investment in cash', '2017-09-15 11:04:11'),
(2, 2, '2017-09-02', 'purchases', 'purchases of furnitures', 'purchases of furnitures', '2017-09-15 11:06:45'),
(4, 3, '2017-09-03', 'general', 'advance office rent', 'advance office rent', '2017-09-15 13:23:20'),
(8, 4, '2017-09-06', 'payments', 'purchases of goods for cash ( 10000 * 40 )', 'purchases of goods for cash ( 10000 * 40 )', '2017-09-15 14:38:51'),
(9, 10, '2017-09-07', 'receipts', 'sales for cash ( 4000 * 60 )', 'sales for cash ( 4000 * 60 )', '2017-09-25 04:58:13'),
(10, 11, '2017-09-12', 'sales', 'sales on credit( 6000 * 60 )', 'sales on credit( 6000 * 60 )', '2017-09-25 05:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `journal_view`
--

CREATE TABLE `journal_view` (
  `id` int(11) NOT NULL,
  `j_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `type` enum('dr','cr') NOT NULL,
  `amount` int(11) NOT NULL,
  `ladger` int(11) NOT NULL,
  `bd` float NOT NULL,
  `cd` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal_view`
--

INSERT INTO `journal_view` (`id`, `j_id`, `account_id`, `type`, `amount`, `ladger`, `bd`, `cd`) VALUES
(1, 1, 7, 'dr', 1000000, 8, 0, 1000000),
(2, 1, 8, 'cr', 1000000, 7, 0, -1000000),
(3, 2, 10, 'dr', 200000, 7, 0, 200000),
(4, 2, 7, 'cr', 200000, 10, 1000000, 800000),
(7, 4, 9, 'dr', 200000, 7, 0, 200000),
(8, 4, 7, 'cr', 200000, 9, 800000, 600000),
(12, 8, 11, 'dr', 400000, 7, 0, 400000),
(13, 8, 7, 'cr', 400000, 11, 600000, 200000),
(14, 9, 7, 'dr', 240000, 12, 200000, 440000),
(15, 9, 12, 'cr', 240000, 7, 0, -240000),
(16, 10, 13, 'dr', 360000, 12, 0, 360000),
(17, 10, 12, 'cr', 360000, 13, -240000, -600000);

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `id` int(11) NOT NULL,
  `details` varchar(255) NOT NULL,
  `narrations` varchar(190) NOT NULL,
  `amount` float NOT NULL,
  `prepare_by` int(11) NOT NULL,
  `auth_by` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transection`
--

INSERT INTO `transection` (`id`, `details`, `narrations`, `amount`, `prepare_by`, `auth_by`, `date`) VALUES
(6, 'capital investment in cash', 'capital investment in cash', 1000000, 0, 0, '2017-09-01'),
(7, 'advance office rent', 'advance office rent', 200000, 0, 0, '2017-09-01'),
(8, 'purchases of furnitures', 'purchases of furnitures', 200000, 0, 0, '2017-09-03'),
(9, 'purchases of goods for cash ( 10000 * 40 )', 'purchases of goods for cash ( 10000 * 40 )', 400000, 0, 0, '2017-09-06'),
(10, 'sales for cash ( 4000 * 60 )', 'sales for cash ( 4000 * 60 )', 240000, 0, 0, '2017-09-13'),
(11, 'sales on credit( 6000 * 60 )', 'sales on credit( 6000 * 60 )', 360000, 0, 0, '2017-09-22'),
(12, 'paid salary for the month', 'paid salary for the month', 80000, 0, 0, '2017-09-29'),
(13, 'paid office rent for the month ( advance adj. 10000 + rent 40000 )', 'paid office rent for the month ( advance adj. 10000 + rent 40000 )', 50000, 0, 0, '2017-09-30'),
(14, 'paid utility bill for the month', 'paid utility bill for the month', 10000, 0, 0, '2017-09-30'),
(15, 'received against credit sales', 'received against credit sales', 200000, 0, 0, '2017-09-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `account_type`
--
ALTER TABLE `account_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_view`
--
ALTER TABLE `journal_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `account_type`
--
ALTER TABLE `account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `journal`
--
ALTER TABLE `journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `journal_view`
--
ALTER TABLE `journal_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `transection`
--
ALTER TABLE `transection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
