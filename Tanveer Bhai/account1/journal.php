<?php include("config.php"); ?>
<?php include("include/header.php"); ?>
<h1>Journal</h1>
<button><a href="entry.php">Add</a></button>
<?php 

	$journals = $db->select("
		SELECT `journal`.*, `journal_view`.*, `accounts`.`name`, `accounts`.`id` as accountsId
		FROM `journal`
		LEFT JOIN `journal_view` ON  `journal`.`id` = `journal_view`.`j_id`
		LEFT JOIN `accounts` ON `journal_view`.`account_id` = `accounts`.`id`
		ORDER BY `journal`.`trans_date`
	");


?>

<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>date</th>
		<th>particulars</th>
		<th>COA ID</th>
		<th>Dr.</th>
		<th>Cr.</th>
		<th>comment</th>
	</tr>
	<?php 
	$totalDr = 0; $totalCr = 0;
	foreach($journals as $journal) { 
	($journal['type'] == 'dr') ? $totalDr = $totalDr + $journal['amount'] : $totalCr = $totalCr + $journal['amount'];
	?>
	<tr>
		<td><?php echo $journal['trans_date']; ?></td>
		<td><?php echo $journal['name']; ?></td>
		<td><?php echo $journal['accountsId']; ?></td>
		<td><?php echo ($journal['type'] == 'dr') ? $journal['amount'] : ''; ?></td>
		<td><?php echo ($journal['type'] == 'cr') ? $journal['amount'] : ''; ?></td>
		<td><?php echo $journal['naration']; ?></td>
	</tr>
	<?php } ?>
	<tr>
		<th colspan="3" > Total</th>
		<th><?php echo $totalDr;?></th>
		<th><?php echo $totalCr;?></th>
		<th></th>
	
	</tr>
	
</table>



<?php include("include/footer.php"); ?>