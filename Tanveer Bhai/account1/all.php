<?php 
include("config.php"); 
include("include/header.php");

?>
<h1>All Report</h1>
<form action="" method="get">
	from: <input type="date" name="from"/>
	to: <input type="date" name="to"/>
	<input type="submit" name="submit" value="submit"/>
</form>
<br/>

<?php 
	if( isset($_GET['submit']) && !empty($_GET['submit'])  ) { 
		extract($_GET);
?>

<!-- journal -->
<h2>Journal</h2>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>date</th>
		<th>particulars</th>
		<th>COA ID</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
	<?php 
	
	$journals = $db->select("
		SELECT `journal`.*, `journal_view`.*, `accounts`.`name`
		FROM `journal`
		LEFT JOIN `journal_view` ON  `journal`.`id` = `journal_view`.`j_id`
		LEFT JOIN `accounts` ON `journal_view`.`account_id` = `accounts`.`id`
		WHERE `journal`.`trans_date` BETWEEN '$from' AND '$to'
		ORDER BY `journal`.`trans_date` ASC, `journal_view`.`id` ASC
	");
	
	$totalDr = 0; $totalCr = 0;
	foreach($journals as $journal) { 
		($journal['type'] == 'dr') ? $totalDr = $totalDr + $journal['amount'] : $totalCr = $totalCr + $journal['amount'];
	
	
	
	if($journal['j_id'] == @$journalId ){
	?>
	<tr>
		<td><?php echo $journal['name']; ?></td>
		<td><?php echo $journal['account_id']; ?></td>
		<td><?php echo ($journal['type'] == 'dr') ? $journal['amount'] : ''; ?></td>
		<td><?php echo ($journal['type'] == 'cr') ? $journal['amount'] : ''; ?></td>
	</tr>
	<tr>
		<th colspan="4"><?php echo $journal['naration']; ?></th>
	</tr>
	<?php
	} else {
	?>
	<tr>
		<td rowspan="3"><?php echo $journal['trans_date']; ?></td>
		<td><?php echo $journal['name']; ?></td>
		<td><?php echo $journal['account_id']; ?></td>
		<td><?php echo ($journal['type'] == 'dr') ? $journal['amount'] : ''; ?></td>
		<td><?php echo ($journal['type'] == 'cr') ? $journal['amount'] : ''; ?></td>
	</tr>
	<?php 
	}
	$journalId = $journal['j_id'];
	} 
	?>
	<tr>
		<th colspan="3" > Total</th>
		<th><?php echo $totalDr;?></th>
		<th><?php echo $totalCr;?></th>
	</tr>
</table>

<!-- end journal -->

<!-- Ledger -->
<h2>Ledger</h2>
<?php 

	$accounts = $db->select("SELECT * FROM `accounts`");

	foreach($accounts as $account){ 
		$accountId = $account['id'];
	
		$ledgers = $db->select("
			SELECT `journal`.`trans_date`, `accounts`.`name`, `journal_view`.`type`,`journal_view`.`amount`
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			LEFT JOIN `accounts` ON `accounts`.`id` = `journal_view`.`ladger` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId 
			ORDER BY `journal`.`trans_date`
		");
		
		$balance = $db->select("
			SELECT `journal`.`trans_date`, `journal_view`.`cd` 
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId 
			ORDER BY `journal`.`trans_date` DESC
			LIMIT 1
		");
		
		($balance) ?  $bd = $balance[0]['cd'] : $bd = '0';
		if( preg_match('/\-/', $bd ) == 0 )
		{
			$bdDr = $bd;
			$bdCr = '';
		} else {
			$bdDr = '';
			$bdCr = abs($bd);
		}
		
	
?>

<h3><?php echo $account['id'].'. '.$account['name']; ?></h3>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>date</th>
		<th>particulars</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
	
	<tr>
		<td></td>
		<td>Balance</td>
		<td><?php echo  $bdDr; ?> </td>
		<td><?php echo  $bdCr; ?> </td>
	</tr>
	
	<?php 
	
	$cd = @$bd; 
	foreach(@$ledgers as $ledger) { 
	
	($ledger['type'] == 'dr') ? $cd = $cd + $ledger['amount'] : $cd = $cd - $ledger['amount'] ;
	
	
	?>
	<tr>
		<td><?php echo $ledger['trans_date']; ?></td>
		<td><?php echo $ledger['name']; ?></td>
		<td><?php echo ($ledger['type'] == 'dr') ? $ledger['amount'] : ''; ?></td>
		<td><?php echo ($ledger['type'] == 'cr') ? $ledger['amount'] : ''; ?></td>
	</tr>
	<?php } ?>
	
	<?php
		if( preg_match('/\-/', $cd ) == 0 )
		{
			$nbdDr = $cd;
			$nbdCr = '';
		} else {
			$nbdDr = '';
			$nbdCr = abs($cd);
		}
		if( preg_match('/\-/', $cd ) == 0 )
		{
			$cdDr = '';
			$cdCr = $cd;
		} else {
			$cdDr = abs($cd);
			$cdCr = '';
		}
		
	?>
	<tr>
		<th></th>
		<th>balance c/d</th>
		<th><?php echo $cdDr; ?></th>
		<th><?php echo $cdCr; ?></th>
	</tr>
	<tr>
		<th></th>
		<th>balance b/d</th>
		<th><?php echo $nbdDr; ?></th>
		<th><?php echo $nbdCr; ?></th>
	</tr>
	
</table>
<?php } // end accounts foreach ?>
<!-- end Ledger -->


<!-- trial balance -->
<h2>Trial Balance</h2>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>COA ID</th>
		<th>particulars</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
<?php
	
	$accounts = $db->select("SELECT * FROM `accounts`");
	$totalDr = 0; $totalCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("
			SELECT sum(`journal_view`.`amount`) as dr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'
		");
		
		$ledgerCr = $db->select("
			SELECT SUM(`journal_view`.`amount`) as cr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'
		");
		$ledgerBd = $db->select("
			SELECT `journal_view`.`cd` 
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId 
			ORDER BY `journal`.`trans_date` DESC
			LIMIT 1
		");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalDr = $totalDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalCr = $totalCr + $cr;
		}
		if($amount !== 0)
		{
?>
	<tr>
		<td><?php echo $account['id']; ?></td>
		<td><?php echo $account['name']; ?></td>
		<td><?php echo $dr; ?></td>
		<td><?php echo $cr; ?></td>
	</tr>
<?php
		} // if amount not zero
	}	// end foreach
?>

	<tr>
		<th colspan="2" > Total </th>
		<th><?php echo $totalDr; ?></th>
		<th><?php echo $totalCr; ?></th>
	</tr>
	
</table>
<!-- end trial balance -->


<!-- Income Statement -->
<h2>Income Statement</h2>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>COA ID</th>
		<th>particulars</th>
		<th>Dr.</th>
		<th>Cr.</th>
	</tr>
<?php
	
	$accounts = $db->select("SELECT `accounts`.*, `account_type`.`name` AS typeName FROM `account_type` LEFT JOIN `accounts` ON `account_type`.`id` = `accounts`.`account_type_id` WHERE `account_type`.`id` = '2'");
	
	$totalReDr = 0; $totalReCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("
			SELECT sum(`journal_view`.`amount`) as dr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'
		");
		
		$ledgerCr = $db->select("
			SELECT SUM(`journal_view`.`amount`) as cr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'
		");
		$ledgerBd = $db->select("
			SELECT `journal_view`.`cd` 
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId 
			ORDER BY `journal`.`trans_date` DESC
			LIMIT 1
		");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalReDr = $totalReDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalReCr = $totalReCr + $cr;
		}
		if($amount !== 0)
		{
?>
	<tr>
		<td><?php echo $account['id']; ?></td>
		<td><?php echo $account['name']; ?></td>
		<td><?php echo $dr; ?></td>
		<td><?php echo $cr; ?></td>
	</tr>
<?php
		} // if amount not zero
	}	// end foreach
?>
	<tr>
		<th></th>
		<th> <i>Total Income</i> </th>
		<th><?php //echo $totalReDr; ?></th>
		<th><?php echo $totalReCr; ?></th>
	</tr>
<?php
	
	$accounts = $db->select("SELECT `accounts`.*, `account_type`.`name` AS typeName FROM `account_type` LEFT JOIN `accounts` ON `account_type`.`id` = `accounts`.`account_type_id` WHERE `account_type`.`id` = '1'");
	
	$totalExDr = 0; $totalExCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("
			SELECT sum(`journal_view`.`amount`) as dr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'
		");
		
		$ledgerCr = $db->select("
			SELECT SUM(`journal_view`.`amount`) as cr
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'
		");
		$ledgerBd = $db->select("
			SELECT `journal_view`.`cd` 
			FROM `journal` 
			LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` 
			WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId 
			ORDER BY `journal`.`trans_date` DESC
			LIMIT 1
		");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalExDr = $totalExDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalExCr = $totalExCr + $cr;
		}
		if($amount !== 0)
		{
?>
	<tr>
		<td><?php echo $account['id']; ?></td>
		<td><?php echo $account['name']; ?></td>
		<td><?php echo $dr; ?></td>
		<td><?php echo $cr; ?></td>
	</tr>
<?php
		} // if amount not zero
	}	// end foreach
?>
	<tr>
		<th></th>
		<th> <i>Total Expense</i> </th>
		<th><?php echo $totalExDr; ?></th>
		<th><?php //echo $totalExCr; ?></th>
	</tr>
<?php

	$net = $totalReCr - $totalExDr;
	$income = 0;
	if( preg_match('/\-/', $net) == 0 )
		{
			$netIncome = $net;
			$income = 1;
		} else {
			$netLoss = abs($net);
		}
	
	if($income == 1){
?>	
	<tr>
		<th colspan="2"> <i>Net Income</i> </th>
		<th><?php echo $netIncome; ?></th>
		<th><?php //echo $totalCr; ?></th>
	</tr>
<?php  } else {  ?>	
	<tr>
		<th colspan="2"> <i>Net Loss</i> </th>
		<th><?php //echo $totalDr; ?></th>
		<th><?php echo $netLoss; ?></th>
	</tr>
<?php } ?>	
<?php
if($income == 1){
	$incomeCr = $totalReCr;
	$incomeDr = $totalExDr + $netIncome;
} else {
	$incomeDr = $totalExDr;
	$incomeCr = $totalReCr + $netLoss;
}
?>
	<tr>
		<th colspan="2"> <i>Total</i> </th>
		<th><?php echo $incomeDr; ?></th>
		<th><?php echo $incomeCr; ?></th>
	</tr>
</table>
<!-- end Income Statement -->


<!-- Balance Sheet -->
<h2>Balance Sheet</h2>
<table border='1' style="width:100%;text-align:left;">
	<tr>
		<th>COA ID</th>
		<th>particulars</th>
		<th>Assets</th>
		<th>Liabilities + Owner's Equity</th>
	</tr>
<?php
	
	// calculate net income or losss
	
	//-- Revene --//
	$accounts = $db->select("SELECT `accounts`.*, `account_type`.`name` AS typeName FROM `account_type` LEFT JOIN `accounts` ON `account_type`.`id` = `accounts`.`account_type_id` WHERE `account_type`.`id` = '2'");
	
	$totalReDr = 0; $totalReCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("SELECT sum(`journal_view`.`amount`) as dr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'");
		
		$ledgerCr = $db->select("SELECT SUM(`journal_view`.`amount`) as cr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'");
		
		@$ledgerBd = $db->select("SELECT `journal_view`.`cd` FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId ORDER BY `journal`.`trans_date` DESC LIMIT 1");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalReDr = $totalReDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalReCr = $totalReCr + $cr;
		}
	}	// end foreach
	
	
	//-- expenses --//
	$accounts = $db->select("SELECT `accounts`.*, `account_type`.`name` AS typeName FROM `account_type` LEFT JOIN `accounts` ON `account_type`.`id` = `accounts`.`account_type_id` WHERE `account_type`.`id` = '1'");
	
	$totalExDr = 0; $totalExCr = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("SELECT sum(`journal_view`.`amount`) as dr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'");
		
		$ledgerCr = $db->select("SELECT SUM(`journal_view`.`amount`) as cr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'");
		
		$ledgerBd = $db->select("SELECT `journal_view`.`cd` FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId ORDER BY `journal`.`trans_date` DESC LIMIT 1");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
		if( preg_match('/\-/', $amount) == 0 )
		{
			$dr = $amount;
			$cr = '';
			$totalExDr = $totalExDr + $dr;
		} else {
			$dr = '';
			$cr = abs($amount);
			$totalExCr = $totalExCr + $cr;
		}
	}	// end foreach
	
	
	$net = $totalReCr - $totalExDr;
	$income = 0;
	if( preg_match('/\-/', $net) == 0 )
		{
			$netIncome = $net;
			$income = 1;
		} else {
			$netLoss = abs($net);
		}
	
	//-- asset --//
	$accounts = $db->select("SELECT `accounts`.*, `account_type`.`name` AS typeName FROM `account_type` LEFT JOIN `accounts` ON `account_type`.`id` = `accounts`.`account_type_id` WHERE `account_type`.`id` = '3' OR `account_type`.`id` = '5'");

	$totalAsset = 0; $totalLiab = 0;
	foreach($accounts as $account)
	{
		$accountId = $account['id'];
		$ledgerDr = $db->select("SELECT sum(`journal_view`.`amount`) as dr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'dr'");
		
		$ledgerCr = $db->select("SELECT SUM(`journal_view`.`amount`) as cr FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` BETWEEN '$from' AND '$to') AND `journal_view`.`account_id` = $accountId AND `journal_view`.`type` = 'cr'");
		
		$ledgerBd = $db->select("SELECT `journal_view`.`cd` FROM `journal` LEFT JOIN `journal_view` ON `journal`.`id` = `journal_view`.`j_id` WHERE (`journal`.`trans_date` < '$from' ) AND `journal_view`.`account_id` = $accountId ORDER BY `journal`.`trans_date` DESC LIMIT 1");
		
		$amount = @$ledgerBd[0]['cd'] + $ledgerDr[0]['dr'] - $ledgerCr[0]['cr'] ;
	
		$totalAmount = abs($amount);
		
			if($account['account_type_id'] == '3'){
				$asset = $totalAmount;
				$liabities = '';
				$totalAsset = $totalAsset + $asset;
			}elseif ($account['account_type_id'] == '5') {
				$liabities = $totalAmount;
				$asset = '';
				$totalLiab = $totalLiab + $liabities;
			}
?>
	<tr>
		<td><?php echo $account['id']; ?></td>
		<td><?php echo $account['name']; ?></td>
		<td><?php echo $asset; ?></td>
		<td><?php echo $liabities; ?></td>
	</tr>
<?php
	}	// end foreach
	
	if($income == 1){ 
		$totalLiab = $totalLiab + $netIncome;
?>
	<tr>
		<td></td>
		<td>Net Income</td>
		<td></td>
		<td><?php echo $netIncome; ?></td>
	</tr>
<?php 
	} else { 
		$totalAsset = $totalAsset + $netLoss;
?>
	<tr>
		<td></td>
		<td>Net Loss</td>
		<td><?php echo $netLoss; ?></td>
		<td></td>
	</tr>
<?php 
	} // end income if condition
?>
	<tr>
		<th></th>
		<th>Total</th>
		<th><?php echo $totalAsset; ?></th>
		<th><?php echo $totalLiab; ?></th>
	</tr>


</table>
<!-- end Balance Sheet -->



<?php
}  // end get from date to date
?>
<br/><br/><br/><br/><br/><br/>


<?php include("include/footer.php"); ?>