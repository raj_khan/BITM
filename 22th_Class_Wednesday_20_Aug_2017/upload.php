<?php
error_reporting(0);
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    echo $check;
    if ($check !== false) {
        echo "File is an image -" . $check["mime"] . ".<br>";
        $uploadOk = 1;
    } else {
        echo "<p class='p2'>File is not an image</p>";
        $uploadOk = 0;
    }
    if (file_exists($target_file)) {
        echo "<p class='p2'>Sorry File alrady exists</p><br>";
        $uploadOk = 0;
    }

    if ($_FILES["fileToUpload"]["size"] > 500000) {
        echo "<p class='p2'>Sorry your File too large</p>";
        $uploadOk = 0;
    }
    if ($imageFileType != "JPG" && $imageFileType != "Jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
        echo "<p class='p2'>Sorry only JPG,JPEG,PNG & GIF are allowed</p>";
        $uploadOk = 0;
    }
    if ($uploadOk == 0) {
        echo "<p class='p2'>Sorry your File was not uploaded</p>";
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "<p class='p1'>The file " . basename($_FILES["fileToUpload"]["name"]) .
            "has been uploaded.</p>";
        } else {
            echo "<p class='p2'>Sorry,there was an error uploading your file.</p>";
        }
    }
}
?>
<style>
    .p1{
        color:green;
    }
    .p2{
        color:red;
    }
</style><html><body><form action="" method="post" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit"  value="Upload Image"name="submit">
        </form></body></html>