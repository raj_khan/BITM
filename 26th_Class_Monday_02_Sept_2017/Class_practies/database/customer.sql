-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2017 at 10:39 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `pass`, `status`) VALUES
(1, 'demo@gmail.com', 'demo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_information`
--

CREATE TABLE `customer_information` (
  `id` int(8) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerGender` varchar(100) NOT NULL,
  `customerAddress` text NOT NULL,
  `customerCountry` varchar(200) NOT NULL,
  `customerMobileNo` int(11) NOT NULL,
  `customerEmail` varchar(200) NOT NULL,
  `customerLanguage` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_information`
--

INSERT INTO `customer_information` (`id`, `customerName`, `customerGender`, `customerAddress`, `customerCountry`, `customerMobileNo`, `customerEmail`, `customerLanguage`) VALUES
(8, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(9, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(10, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(11, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(12, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(13, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(14, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(15, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(16, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(17, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English'),
(18, 'Raj Khan', 'Male', 'South Badda', 'Bangladesh', 1752046668, 'rajbsd.com@gmail.com', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `suplier_information`
--

CREATE TABLE `suplier_information` (
  `id` int(10) NOT NULL,
  `suplierName` varchar(200) NOT NULL,
  `suplierGender` varchar(200) NOT NULL,
  `suplierAddress` varchar(200) NOT NULL,
  `suplierCountry` varchar(200) NOT NULL,
  `suplierMobileNo` int(10) NOT NULL,
  `suplierEmail` varchar(200) NOT NULL,
  `suplierLanguage` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier_information`
--

INSERT INTO `suplier_information` (`id`, `suplierName`, `suplierGender`, `suplierAddress`, `suplierCountry`, `suplierMobileNo`, `suplierEmail`, `suplierLanguage`) VALUES
(2, 'Raj Khan', 'Male', 'KhilAAAkhet', 'Bangladesh', 1752046668, 'raj@gmail.com', 'English'),
(3, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi'),
(4, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi'),
(5, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi');

-- --------------------------------------------------------

--
-- Table structure for table `user_registration`
--

CREATE TABLE `user_registration` (
  `id` int(10) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userGender` varchar(100) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userPass` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_registration`
--

INSERT INTO `user_registration` (`id`, `userName`, `userGender`, `userEmail`, `userPass`) VALUES
(1, 'raj', 'male', 'email', 'password'),
(2, 'Raj Khan', 'Male', 'raj@gmail.com', '12345678');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_information`
--
ALTER TABLE `customer_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suplier_information`
--
ALTER TABLE `suplier_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_registration`
--
ALTER TABLE `user_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_information`
--
ALTER TABLE `customer_information`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `suplier_information`
--
ALTER TABLE `suplier_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_registration`
--
ALTER TABLE `user_registration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
