 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../js/main.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../asset/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('select[name = "prod_catogory"]').select2();
    });


    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Customer', 8],
            ['Suplier', 8],
            ['Item', 8],
        ]);

        var options = {
            title: 'Shop Details'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>

</body>
</html>