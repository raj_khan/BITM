<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include 'config/config.php';

include '../admin/inc/header.php';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <span class="glyphicon glyphicon-bookmark"></span> Welcome To POS Admin Panel
                    </h3>
                </div>
                <div class="panel-body">
                    <?php include 'inc/menu.php' ?>


                    <!--Chart-->
                    <center>
                        <div class="text-center" id="piechart" style="width: 900px; height: 500px;"></div>
                    </center>



                    <a style="margin-top: 100px;" href="https://www.linkedin.com/in/raj-khan/" class="btn btn-primary btn-lg btn-block" role="button" target="_blank"> Copyright  &copy; 2017, Raj Khan All Right Reserved
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
include '../admin/inc/footer.php';
?>