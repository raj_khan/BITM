<?php
include '../admin/inc/header.php';
include '../admin/config/config.php';

include '../admin/inc/menu.php';
?>

<?php
if (isset($_GET['suplier_id'])) {
    $id = $_GET['suplier_id'];

    $query = mysqli_query($con, "SELECT * FROM `suplier_information` WHERE id = $id");
    $dataForEdit = mysqli_fetch_array($query, MYSQLI_ASSOC);
}

if (isset($_POST['submit'])) {
    $suplierName = $_POST['suplierName'];
    $suplierGender = $_POST['suplierGender'];
    $suplierAddress = $_POST['suplierAddress'];
    $suplierAddress = mysqli_real_escape_string($con, $suplierAddress);
    $suplierCountry = $_POST['suplierCountry'];
    $suplierMobileNo = $_POST['suplierMobileNo'];
    $suplierEmail = $_POST['suplierEmail'];
    $suplierLanguage = $_POST['suplierLanguage'];


    $product_insert_query = "UPDATE `suplier_information` SET `suplierName` = '$suplierName', `suplierGender` = '$suplierGender', `suplierAddress` = '$suplierAddress', `suplierCountry` = '$suplierCountry', `suplierMobileNo` = '$suplierMobileNo', `suplierEmail` = '$suplierEmail', `suplierLanguage` = '$suplierLanguage' WHERE `suplier_information`.`id` = $id";




    $insert_query = mysqli_query($con, $product_insert_query);

    if ($insert_query) {

        $error = "Success! Update your information ";
        header("location:suplier_view.php?status=ok");
    } else {
        $error = "Error! Could not Update your Information";
    }
}
?>


<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>

<h3 class="text-center text-info" style="font-weight: 700;">Update Supplier Information</h3>
<hr>

<div class="container">
    <div class="row">
        <form class="form-horizontal" action="" method="post">
            <fieldset>
                <!-- Form Name -->

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="Name">Name</label>  
                    <div class="col-md-5">
                        <input id="Name" value="<?php echo $dataForEdit['suplierName']; ?>" name="suplierName" type="text" placeholder="Suplier Name" class="form-control input-md" required="">

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label"  for="gender">Gender</label>
                    <div class="col-md-4"> 
                        <label class="radio-inline" for="gender-0">
                            <input type="radio" name="suplierGender" id="gender-0" value="<?php echo $dataForEdit['suplierGender']; ?>" checked="checked">
                            Male
                        </label> 
                        <label class="radio-inline" for="gender-1">
                            <input type="radio" name="suplierGender" id="gender-1" value="<?php echo $dataForEdit['suplierGender']; ?>">
                            Female
                        </label>
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label"  for="address">Address</label>
                    <div class="col-md-4">                     
                        <textarea value="" class="form-control" id="address" name="suplierAddress" placeholder="Address"><?php echo $dataForEdit['suplierAddress']; ?></textarea>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="country">Country</label>
                    <div class="col-md-5">
                        <select id="country" name="suplierCountry" class="form-control">
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Aland Islands">Aland Islands</option>
                            <option value="Algeria">Albania</option>
                            <option value="">Algeria</option>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="mobilenumber">Mobile Number</label>  
                    <div class="col-md-5">
                        <input id="mobilenumber" value="<?php echo $dataForEdit['suplierMobileNo']; ?>" name="suplierMobileNo" type="text" placeholder="Mobile Number" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="emailId">Email Id</label>  
                    <div class="col-md-6">
                        <input id="emailId" value="<?php echo $dataForEdit['suplierEmail']; ?>" name="suplierEmail" type="text" placeholder="user@domain.com" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Select Multiple -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="languages">Languages Known</label>
                    <div class="col-md-5">
                        <select id="languages" name="suplierLanguage" class="form-control">
                            <option value="English">English</option>
                            <option value="Hindi">Hindi</option>
                            <option value="Malayalam">Malayalam</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                </div>


                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Password</label>
                    <div class="col-md-5">
                        <input id="passwordinput" name="passwordinput" type="password" placeholder="" class="form-control input-md" required="">
                        <span class="help-block">max 8 characters</span>
                    </div>
                </div>




                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Submit</button>
                        <input type="hidden" value = "<?php echo $dataForEdit['id']; ?>" name="prodId" class="form-control">
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>

<?php include '../admin/inc/footer.php'; ?>