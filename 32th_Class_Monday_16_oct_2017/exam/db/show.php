<?php include './inc/header.php'; ?>

<?php

function __autoload($class) {
    include_once($class . ".php");
}

$obj = new oopCrud;

if (isset($_REQUEST['status'])) {
    echo"Your Data Successfully Updated";
}

if (isset($_REQUEST['status_insert'])) {
    echo"<h1>Your Data Successfully Inserted</h1>";
}

if (isset($_REQUEST['del_id'])) {
    if ($obj->deleteData($_REQUEST['del_id'], "students")) {

        echo"<h1>Your Data Successfully Deleted</h1>";
    }
}
?>
<div class="container">
    <div class="btn-group">
        <button class="btn">
            <a href="insert.php"style="text-decoration: none;">Insert</a>
        </button>
    </div>
    <h3 class="text-center text-primary">All The Data</h3>
    <table width="750" border="1" class="table table-striped custab">
        <tr class="success">
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scope="col">Action</th>
        </tr>
        <?php
        foreach ($obj->showData("students") as $value) {
            extract($value);
            echo <<<show
 <tr class="">
 <td>$name</td>
 <td>$email</td>
 <td>$mobile</td>
 <td>$address</td>
 <td class="text-center">
     <a class='btn btn-info btn-xs' href="update.php?id=$id"><span class="glyphicon glyphicon-edit"></span> Edit
         </a> 
             <a href="show.php?del_id=$id" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del
                 </a>
 </td>
 
 </tr>
show;
        }
        ?>
        <tr class="">
            <th scope="col" colspan="5" align="right">
        <div class="btn-group">
            <button class="btn"><a href="insert.php" style="text-decoration: none;">Insert New Data</a></button>
        </div>
        </th>

        </tr>
    </table>
</div>

<?php include './inc/footer.php'; ?>