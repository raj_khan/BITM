<?php
session_start();
if (!isset($_SESSION["s_id"])) {
    header("location:login/index.php");
}
include 'inc/header.php';
include 'config/config.php';
?>

<?php
if (isset($_POST['submit'])) {
    $itemSupplierName = $_POST['itemSupplierName'];
    $itemName = $_POST['itemName'];
    $itemDescription = $_POST['itemDescription'];
    $customerAddress = mysqli_real_escape_string($con, $itemDescription);

    $barcode = $_POST['barcode'];
    $itemQuantity = $_POST['itemQuantity'];
    $itemDeliveryQuantity = $_POST['itemDeliveryQuantity'];
    $itemDueQuantity = $_POST['itemDueQuantity'];
    $itemDueQuantity = $_POST['itemDueQuantity'];
    $itemPerPrice = $_POST['itemPerPrice'];
    $directOrderrate = $_POST['directOrderRate'];
    $nationalProgramRate = $_POST['nationalProgramRate'];
    $specialProgramRate = $_POST['specialProgramRate'];
    $productDiscountRate = $_POST['productDiscountRate'];


    $itemSalePrice = $_POST['itemSalePrice'];
    $marketRetailPrice = $_POST['marketRetailPrice'];
    $offerProgram = $_POST['offerProgram'];
    $offerProgramStartDate = $_POST['offerProgramStartDate'];
    $offerProgramEndDate = $_POST['offerProgramEndDate'];
    $itemPaid = $_POST['itemPaid'];
    $itemDue = $_POST['itemDue'];
    $itemTotalPrice = $_POST['itemTotalPrice'];
    $productPicPath = $_POST['productPicPath'];
    $itemProductDate = $_POST['itemProductDate'];




    $item_insert_query = "INSERT INTO `item` (`itemId`, `itemSupplier`, `itemName`, `itemDescription`, `barcode`, `itemQuantity`, `itemDeliveryQuantity`, `itemDueQuantity`, `itemPerPrice`, `directOrderrate`, `nationalProgramRate`, `specialProgramRate`, `productDiscountRate`, `itemSalePrice`, `marketRetailPrice`, `offerProgram`, `offerProgramStartDate`, `offerProgramEndDate`, `itemPaid`, `itemDue`, `itemTotalPrice`, `productPicPath`, `itemProductDate`) VALUES (NULL, '$itemSupplierName', '$itemName', '$itemDescription', '$barcode', '$itemQuantity', '$itemDeliveryQuantity', '$itemDueQuantity', '$itemPerPrice', '$directOrderrate', '$nationalProgramRate', '$specialProgramRate', '$productDiscountRate', '$itemSalePrice', '$marketRetailPrice', '$offerProgram', '$offerProgramStartDate', '$offerProgramEndDate', '$itemPaid', '$itemDue', '$itemTotalPrice', '$productPicPath', '$itemProductDate')";





    $insert_query = mysqli_query($con, $item_insert_query);

    if ($insert_query) {

        $error = "Success! Upload your information ";
    } else {
        $error = "Error! Could not Upload your Information";
    }
}
?>


<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>



<!--Form-->
<div class="container">
    <div class="row">
        <h3 class="text-center text-info" style="font-weight: 700;">Insert Your Item Here</h3>
        <hr>
        <form class="form-horizontal" action="" method="post">
            <fieldset>
                <!-- Form Name -->

                <!-- item Supplier Name-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemSupplierName">Item Supplier Name</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemSupplierName" name="itemSupplierName" type="text" placeholder="item Supplier Name" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- item  Name-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemName">Item Name</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemName" name="itemName" type="text" placeholder="item Name" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Item Description -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemDescription">Item Description</label>
                    <div class="col-md-4">                     
                        <textarea class="form-control" id="itemDescription" name="itemDescription" placeholder="item Description"></textarea>
                    </div>
                </div>


                <!-- Barcode-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="barcode">Bar Code</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="barcode" name="barcode" type="text" placeholder="barcode" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- item Quantity-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemQuantity">item Quantity</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemQuantity" name="itemQuantity" type="number" placeholder="item Quantity" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- item Delivery Quantity-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemDeliveryQuantity">item Delivery Quantity</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemDeliveryQuantity" name="itemDeliveryQuantity" type="number" placeholder="item Delivery Quantity" class="form-control input-md" required="">

                    </div>
                </div>



                <!-- item Due Quantity-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemDueQuantity">item Due Quantity</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemDeliveryQuantity" name="itemDueQuantity" type="number" placeholder="item Due Quantity" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- item Per Price-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemPerPrice">Item Per Price</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemPerPrice" name="itemPerPrice" type="number" placeholder="Item Per Price" class="form-control input-md" required="">

                    </div>
                </div>



                <!-- Direct Order Rate-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="DirectOrderRate">Direct Order Rate</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="DirectOrderRate" name="directOrderRate" type="number" placeholder="Direct Order Rate" class="form-control input-md" required="">

                    </div>
                </div>




                <!-- National Program Rate-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="nationalProgramRate">National Program Rate</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="nationalProgramRate" name="nationalProgramRate" type="number" placeholder="National Program Rate" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Special Program Rate-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="specialProgramRate">Special Program Rate</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="specialProgramRate" name="specialProgramRate" type="number" placeholder="Special Program Rate" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Product Discount Rate-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="productDiscountRate">Product Discount Rate</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="productDiscountRate" name="productDiscountRate" type="number" placeholder="Product Discount Rate" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Item Sale Price-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="ItemSalePrice">Item Sale Price</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="ItemSalePrice" name="itemSalePrice" type="number" placeholder="Item Sale Price" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Market Retail Price-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="marketRetailPrice">Market Retail Price</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="marketRetailPrice" name="marketRetailPrice" type="number" placeholder="Market Retail Price" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Offer Program-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="offerProgram">Offer Program</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="offerProgram" name="offerProgram" type="text" placeholder="Offer Program" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Offer Program Start Date-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="offerProgramStartDate">Offer Program Start Date</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="offerProgramStartDate" name="offerProgramStartDate" type="date" placeholder="Offer Program Start Date" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Offer Program End Date-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="offerProgramEndDate">Offer Program End Date</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="offerProgramEndDate" name="offerProgramEndDate" type="date" placeholder="Offer Program End Date" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Item Paid-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemPaid">Item Paid</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemPaid" name="itemPaid" type="number" placeholder="itemPaid" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Item Due-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemDue">Item Due</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemDue" name="itemDue" type="number" placeholder="itemDue" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Item Total Price-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemTotalPrice">Item Total Price</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemTotalPrice" name="itemTotalPrice" type="number" placeholder="Item Total Price" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Product Picture Path-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="productPicPath">Product Image</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="productPicPath" name="productPicPath" type="file"  class="form-control input-md" required="">

                    </div>
                </div>




                <!-- Item Product Date-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="itemProductDate">Item Product Date</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="itemProductDate" name="itemProductDate" type="date"  class="form-control input-md" placeholder="item Product Date" required="">

                    </div>
                </div>





                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>



<?php include 'inc/footer.php'; ?>