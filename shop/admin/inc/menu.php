<div class="panel-body">
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 text-center menu_bar">
            <a href="index.php" class="btn btn-primary btn-sm" role="button"  ><span class="glyphicon glyphicon-user"></span> <br/>Home</a>
            <a href="itemInsert.php" class="btn btn-success btn-sm" role="button"  ><span class="glyphicon glyphicon-file"></span> <br/>Add Item</a>
            <a href="addCustomer.php" class="btn btn-danger btn-sm" role="button"  ><span class="glyphicon glyphicon-bookmark"></span> <br/>Add Customer</a>
            <a href="addSuplier.php" class="btn btn-info btn-sm" role="button"  ><span class="glyphicon glyphicon-picture"></span> <br/>Add Supplier</a>
            <a href="itemView.php" class="btn btn-warning btn-sm" role="button"  ><span class="glyphicon glyphicon-file"></span> <br/>View Item</a>
            <a href="customer_view.php" class="btn btn-warning btn-sm" role="button"  ><span class="glyphicon glyphicon-bookmark"></span> <br/>View Customer</a>
            <a href="suplier_view.php" class="btn btn-success btn-sm" role="button"  ><span class="glyphicon glyphicon-picture"></span> <br/>View Supplier</a>
            <a href="changePass.php" class="btn btn-primary btn-sm" role="button"  ><span class="glyphicon glyphicon-signal"></span> <br/>Change Password</a>
            <a href="logout.php" class="btn btn-danger btn-sm" role="button"  ><span class="glyphicon glyphicon-comment"></span> <br/>Logout</a>
        </div>

    </div>

</div>