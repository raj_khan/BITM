
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>POS</title>

        <!-- Bootstrap -->
        <link rel="icon" href="asset/img/favicon.png" type="image/x-icon"/>
        <link rel="shortcut icon" href="asset/img/favicon.png" type="image/x-icon"/>
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!--Data Table css-->
        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../asset/css/style.css" rel="stylesheet">

        <link href="asset/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container-fluid">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">
                            <span class="glyphicon glyphicon-bookmark"></span> Welcome To POS Admin Panel
                        </h3>
                    </div>
                    <div class="panel-body">
                        <?php include 'inc/menu.php'; ?>
                    </div>
