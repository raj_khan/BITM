<?php
include '../admin/config/config.php';
include '../admin/inc/header.php';
?>

<h3 class="text-center text-info" style="font-weight: 700;">Customer Information Table</h3>
<hr>

<?php
if (isset($_GET['search'])) {
    $searchbar = $_GET['searchBox'];


    $query = "SELECT * FROM `customer_information` WHERE `customerName` LIKE '$searchbar' ";
    $searchQuery = mysqli_query($con, $query);
    $insertSearchQuery = mysqli_fetch_array($searchQuery, MYSQLI_ASSOC);



}
?>

<form action="" method="get">
    <div class="container" style="margin-bottom: 130px;">
        <div class="row">
            <h2>Search Box</h2>
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" name="searchBox" class="  search-query form-control" placeholder="Search" />
                    <span class="input-group-btn">
                        <button name="search" class="btn btn-danger" type="submit">
                            <span class=" glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="container">
    <div class="col-md-12">

        <?php
        if (@$insertSearchQuery) {
            ?>
            <table id="customerData" class="display table table-striped custab" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Customer Name</th>
                        <th>Gender</th>
                        <th>Address</th>
                        <th>Country</th>
                        <th>Mobile No.</th>
                        <th>Email ID</th>
                        <th>Language</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
//$sql_menu = mysqli_query($con, "SELECT * FROM `customer_information` ORDER BY `customer_information`.`id` ASC");
                    ?>
                    <tr>
                        <td><?php echo $insertSearchQuery['id']; ?></td>
                        <td><?php echo $insertSearchQuery['customerName']; ?></td>
                        <td><?php echo $insertSearchQuery['customerGender']; ?></td>
                        <td><?php echo $insertSearchQuery['customerAddress']; ?></td>
                        <td><?php echo $insertSearchQuery['customerCountry']; ?></td>
                        <td><?php echo $insertSearchQuery['customerMobileNo']; ?></td>
                        <td><?php echo $insertSearchQuery['customerEmail']; ?></td>
                        <td><?php echo $insertSearchQuery['customerLanguage']; ?></td>
                        <td><a class='btn btn-info btn-xs' href="update.php?customer_id=<?php echo $data['id']; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a> 
                            <a href="deleteProduct.php?customer_id=<?php echo $data['id']; ?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
                    </tr>


                </tbody>
            </table>
<?php } else {
    echo 'no data found!!';
} ?>

    </div>
</div>

<?php
include '../admin/inc/footer.php';
?>