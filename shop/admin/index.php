<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include 'config/config.php';

include '../admin/inc/header.php';
?>



<!--Chart-->

<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <center>
                <div class="text-center" id="piechart" style="width: 900px; height: 500px;"></div>
            </center>
        </div>
    </div>
</div>







<?php
include '../admin/inc/footer.php';
?>