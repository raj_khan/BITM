<?php
session_start();
if (!isset($_SESSION["s_id"])) {
    header("location:login/index.php");
}
include 'inc/header.php';
include 'config/config.php';

?>

<?php
if (isset($_POST['submit'])) {
    $suplierName = $_POST['suplierName'];
    $suplierGender = $_POST['suplierGender'];
    $suplierAddress = $_POST['suplierAddress'];
    $suplierAddress = mysqli_real_escape_string($con, $suplierAddress);
    $suplierCountry = $_POST['suplierCountry'];
    $suplierMobileNo = $_POST['suplierMobileNo'];
    $suplierEmail = $_POST['suplierEmail'];
    $suplierLanguage = $_POST['suplierLanguage'];



    $product_insert_query = "INSERT INTO `suplier_information` (`id`, `suplierName`, `suplierGender`, `suplierAddress`, `suplierCountry`, `suplierMobileNo`, `suplierEmail`, `suplierLanguage`) VALUES(NULL, '$suplierName', '$suplierGender', '$suplierAddress', '$suplierCountry', '$suplierMobileNo', '$suplierEmail', '$suplierLanguage')";





    $insert_query = mysqli_query($con, $product_insert_query);

    if ($insert_query) {

        $error = "Success! Upload your information ";
    } else {
        $error = "Error! Could not Upload your Information";
    }
}
?>


<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>







<h3 class="text-center text-info" style="font-weight: 700;">Supplier Registration Form</h3>
<hr>

<div class="container">
    <div class="row">
        <form class="form-horizontal" action="" method="post">
            <fieldset>
                <!-- Form Name -->

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="Name">Name</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="Name" name="suplierName" type="text" placeholder="Supplier Name" class="form-control input-md" required="">

                    </div>
                </div>


                <!-- Multiple Radios (inline) -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="gender">Gender</label>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12"> 
                        <label class="radio-inline" for="gender-0">
                            <input type="radio" name="suplierGender" id="gender-0" value="Male" checked="checked">
                            Male
                        </label> 
                        <label class="radio-inline" for="gender-1">
                            <input type="radio" name="suplierGender" id="gender-1" value="Female">
                            Female
                        </label>
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="address">Address</label>
                    <div class="col-md-4">                     
                        <textarea class="form-control" id="address" name="suplierAddress" placeholder="Address"></textarea>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="country">Country</label>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <select id="country" name="suplierCountry" class="form-control">
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Aland Islands">Aland Islands</option>
                            <option value="Algeria">Albania</option>
                            <option value="">Algeria</option>
                        </select>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="mobilenumber">Mobile Number</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="mobilenumber" name="suplierMobileNo" type="text" placeholder="Mobile Number" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="emailId">Email Id</label>  
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="emailId" name="suplierEmail" type="text" placeholder="user@domain.com" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Select Multiple -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="languages">Languages Known</label>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <select id="languages" name="suplierLanguage" class="form-control">
                            <option value="English">English</option>
                            <option value="Hindi">Hindi</option>
                            <option value="Malayalam">Malayalam</option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                </div>


                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label" for="passwordinput">Password</label>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                        <input id="passwordinput" name="passwordinput" type="password" placeholder="" class="form-control input-md" required="">
                        <span class="help-block">max 8 characters</span>
                    </div>
                </div>



                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 col-lg-4 col-sm-6 col-xs-12 control-label "for="submit"></label>
                    <div class="col-md-4">
                        <button id="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>

<?php include 'inc/footer.php'; ?>