-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2017 at 04:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `pass`, `status`) VALUES
(1, 'demo@gmail.com', 'demo1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_information`
--

CREATE TABLE `customer_information` (
  `id` int(8) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerGender` varchar(100) NOT NULL,
  `customerAddress` text NOT NULL,
  `customerCountry` varchar(200) NOT NULL,
  `customerMobileNo` int(11) NOT NULL,
  `customerEmail` varchar(200) NOT NULL,
  `customerLanguage` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suplier_information`
--

CREATE TABLE `suplier_information` (
  `id` int(10) NOT NULL,
  `suplierName` varchar(200) NOT NULL,
  `suplierGender` varchar(200) NOT NULL,
  `suplierAddress` varchar(200) NOT NULL,
  `suplierCountry` varchar(200) NOT NULL,
  `suplierMobileNo` int(10) NOT NULL,
  `suplierEmail` varchar(200) NOT NULL,
  `suplierLanguage` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier_information`
--

INSERT INTO `suplier_information` (`id`, `suplierName`, `suplierGender`, `suplierAddress`, `suplierCountry`, `suplierMobileNo`, `suplierEmail`, `suplierLanguage`) VALUES
(2, 'Raj Khan', 'Male', 'KhilAAAkhet', 'Bangladesh', 1752046668, 'raj@gmail.com', 'English'),
(3, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi'),
(4, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi'),
(5, 'Raj Khan', 'Male', 'Khilkhet', 'Afghanistan', 1752046668, 'raj@gmail.com', 'Hindi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`) VALUES
(19, 'Asp.net'),
(2, 'php'),
(3, 'HTML'),
(4, 'CSS'),
(17, 'c++'),
(7, 'java ajax'),
(18, 'ajax');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `body` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `firstname`, `lastname`, `email`, `body`, `status`, `date`) VALUES
(4, 'hasan', 'mahadi', 'tomaloffical@gmail.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', '1', '2016-11-24 17:58:00'),
(5, 'Arif', 'Hasan', 'ahasan_jsr@yahoo.com', 'Thanks and great job.', '1', '2016-11-24 19:52:55'),
(6, 'Nur E Alom', 'Siddique', 'nur@bitbirds.com', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the ', '', '2016-11-27 07:59:36'),
(7, 'ArianKer', 'ArianKerBV', 'xeruzonyzolypyu@gmail.com', 'Ð¿Ð¾ÑƒÑ€Ð¾Ñ‡Ð½Ð¾Ðµ Ð¿Ð»Ð°Ð½Ð¸Ñ€Ð¾Ð²Ð°Ð½Ð¸Ðµ Ð¿Ð¾ Ð¼Ð°Ñ‚ÐµÐ¼Ð°Ñ‚Ð¸ÐºÐµ 2 ÐºÐ»Ð°ÑÑ Ñ„Ð³Ð¾Ñ Ð¿Ð¾Ð¿Ñƒ', '', '2017-01-13 01:39:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_footer`
--

CREATE TABLE `tbl_footer` (
  `id` int(11) NOT NULL,
  `note` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_footer`
--

INSERT INTO `tbl_footer` (`id`, `note`) VALUES
(1, 'Copyright Zsof IT Solution Pvt . Ltd .');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `name`, `body`) VALUES
(8, 'About us ', '<h3>WHO WE ARE?</h3>\r\n<p align=\"center\"><strong>&ldquo; We Build Smarter and Highly Usable Stuff! &rdquo;</strong></p>\r\n<p align=\"center\"><strong>bitm</strong></p>\r\n<p>Zsoft IT Solution Pvt.Ltd is an IT enabled Web and Mobile Application Development service providing company in the market since 2000. Zsoft IT Solution Pvt.Ltd provides two major types of service. One is Web Solution that consists of high quality Web Design &amp; Development, Website Maintenance, Social Networking Platform Development, E-Commerce Solutions, CMS Solutions, e-Prochar as E-mail Marketing Service, Managed Web Hosting Service and Domain Registration Service. Other one is Mobile Application Development in both Android &amp; iOS Platform.</p>\r\n<h3>We Believe</h3>\r\n<p align=\"center\"><strong>&ldquo; Unleashing the Possibilities &rdquo;</strong></p>\r\n<p>How? We help individuals/inventors/entrepreneurs/designers/developers/fresh-thinkers to achieve their dream and goal by showing them how to increase their overall productivity, identity and belief in their ability towards success. As you read our testimonials you will find that many of our clients identify our rigorous accountability approach as well as our ability to increase their self-belief as the driving forces behind them remaining focused on pursuing and achieving their dreams and goals.</p>\r\n<h3>OUR VISION</h3>\r\n<p>To be the leading company inWeb &amp; Mobile Application Development Company that empowers individuals&amp; companies to transform their ideas to reality.</p>\r\n<h3>OUR MISSION</h3>\r\n<p>Our mission is 100% client satisfaction and we focused on versatile project management with aim to provide Web &amp; Mobile App solutions with global acceptability.</p>'),
(13, 'Our Service', '<h1>WHAT WE DO?</h1>\r\n<p>Zsoft IT Solution Pvt.Ltd is an IT consulting and development company in service on the market since 2002. We provides high-quality development such as web design and development, database design &amp; integration, programming, web site maintenance, e-commerce solutions and application program development.</p>\r\n<h3>WEB DESIGN &amp; DEVELOPMENT</h3>\r\n<p>ZSOFT IT SOLUTION PVT.LTD is one of the leading web design&amp; development company in Bangladesh.We often act as an extension of your business. We develop static html websites to dynamic web portals, e-commerce portals,CMS based websites and social networking platforms.</p>\r\n<h3>WEB HOSTING SERVICE</h3>\r\n<p>Zsoft IT Solution Pvt.Ltd is the pioneer in the web hosting industry of Bangladesh and today serving more than 2000 websites on a number of USA based Linux and Windows servers. Web hosting service range includes managed dedicated servers, VPS, cloud servers, reseller hosting and shared hosting.</p>\r\n<h3>E-COMMERCE SOLUTION</h3>\r\n<p>Our E-commerce solutions ensure your business growth without any technical experience at your end. Simply update us your requirement &amp; we are here to grow your business.</p>\r\n<h3>MOBILE APPLICATION DEVELOPMENT</h3>\r\n<p>We work to convert ideas into real mobile apps. Zsoft IT Solution Pvt.Ltd starts mobile application development in the year of 2010. Web services based iOS and Android application development is the prime focus of Zsoft IT Solution Pvt.Ltd mobile application development wing.</p>\r\n<h3>OFFSHORE SOLUTIONS</h3>\r\n<p>Thinking about partnering with a reliable and trust worthy web&amp; mobile applicationdevelopmentcompany to outsource your projects offshore? We have a proven track of 13+ years doing outsourcing works for many of the world\'s largest agencies. They have found us exceptional and became long standing returning clients.</p>\r\n<h3>Domain Registration</h3>\r\n<p>Zsoft IT Solution Pvt.Ltd provides Domain Registration services since 2000. We say, think of a name that\'s short, catchy and easy to remember. This could be your brand or personal name or a term people will use to search for your site.</p>\r\n<h3>Content Management Systems (CMS)</h3>\r\n<p>Content Management Systems (CMS) are the most popular solutions that empower clients to manage their websites themselves. We design CMS systems that are completely self-explanatory and need almost no training to manage.</p>\r\n<p>&nbsp;</p>'),
(14, 'WHY CHOOSE US?', '<h5 align=\"center\">Our experienced marketers, talented designers and skilled programmers work together to uncover your goals and understand your business need and market. We develop and build web&amp; mobile solutions designed to attract your target customers.</h5>\r\n<p>This ensures that we have a clearer understanding of what steps should be taken to ensure your specific marketing needs are fully met.</p>\r\n<h3>WORK FORCE</h3>\r\n<p>We\'ve spent years cultivating the best talent in the web/mobile design &amp; development industry. Our team is passionate about what they do and it shows in our work.</p>\r\n<h3>Our Promise</h3>\r\n<p>At Zsoft IT Solution Pvt.Ltd, we have a long-standing reputation for industry-leading support. Our support team is the backbone of our company and transparency is our Promise.</p>\r\n<h3>Proactive Monitoring</h3>\r\n<p>We know that it\'s critical for your position to stay online. Our team pro-actively monitors your server 24x7 for issues, and if something occurs, we usually fix it before you even know a problem existed. We monitor everything: software, hardware and our network.</p>\r\n<h3>Perfection in Design Principles</h3>\r\n<p>Our talented pool of web&amp; mobile developers work in close coordination with the clients to ensure that the website/app satisfies the organization objectives &amp; search engines principles with perfection in designing principles and ease of navigation</p>'),
(15, 'Download  Project', '<h4>CMS Blog With PHP OOP</h4>\r\n<p><a href=\"https://sourceforge.net/p/php-oop-cms-blog/\" rel=\"nofollow\"><img src=\"https://sourceforge.net/sflogo.php?type=13&amp;group_id=2782715\" alt=\"Download PHP OOP CMS BLOG\" /></a></p>\r\n<br />\r\n<hr style=\"width: 100%;\" width=\"100%\" />\r\n<p>&nbsp;</p>\r\n<h4>Php OOP Login-Register System</h4>\r\n<p><a href=\"https://sourceforge.net/p/php-oop-login-register-system/\" rel=\"nofollow\"><img src=\"https://sourceforge.net/sflogo.php?type=11&amp;group_id=2762699\" alt=\"Download Php OOP Login-Register System tutorial\" /></a></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `id` int(11) NOT NULL,
  `cat` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `authour` varchar(100) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `cat`, `title`, `body`, `image`, `authour`, `tags`, `date`, `user_id`) VALUES
(18, 3, 'Web Design & Development ', '<p>Zsoft IT Solution Pvt.ltd uses a Rapid Web Development approach to build its web solutions. Organizations have their unique needs and one solution may not fit all the bills. Zsoft IT Solution Pvt.ltd web design &amp; development is the ideal solution for such organizations. Custom web development is what we love, and live every day.<br /><br />Service area of Zsoft IT Solution Pvt.ltd is vast. Zsoft IT Solution Pvt.ltd has successfully established its reputation as a very well-known web design and development company in BD. It provides affordable web design &amp; web development services in BD with expert customer support. We meet project deadline very well and fulfill client&rsquo;s requirement within that time too. For best web design in Bangladesh you can rely on us. Zsoft IT Solution Pvt.ltd team will amaze you will with their talent and creativity. If you need to generate idea we can help. If you have already made a plan or generated an idea we will give life to your idea and help you fuel it with our expertise.<br /><br />Goal of Zsoft IT Solution Pvt.ltd is to achieve client\'s full satisfaction. Now it has become the best Web Design and Development Company in Bangladesh. Zsoft IT Solution Pvt.ltd has worked with 700+ clients all over the world. Zsoft IT Solution Pvt.ltd is the most reliable web design and development company in BD<br /><br /></p>', 'upload/03cee9ede3.jpg', 'admin', 'Web Design & Development Company in Bangladesh', '2016-11-16 18:11:46', 29),
(17, 2, 'Web Application Development', '<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 500 company or a small start-up.</p>\r\n<p>&nbsp;</p>\r\n<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 500 company or a small start-up.</p>', 'upload/f6d4dca880.jpg', 'admin', 'Web Application Development', '2016-11-16 18:11:29', 29),
(21, 2, 'Content Management System', '<p>Content management systems (CMS), such as Wordpress and Drupal allow website owners to easily present, publish, edit and modify the content on a website from a centralized interface. A CMS gives you direct access to the content on your web site. A major advantage of using a CMS is that it requires almost \'no technical skill to manage\'. CMS based dynamic websites are database driven so they\'re faster than static websites.</p>\r\n<p>&nbsp;</p>\r\n<p>Zsoft IT Solution Pvt.ltd &nbsp;provides customized CMS solutions to individuals and enterprises to fulfill their unique business needs. Require Joomla tweaks, Drupal updates, Wordpress development or little Magento magic?</p>\r\n<p>&nbsp;</p>\r\n<p>Our content management solutions allow you to do everything &ndash; quickly and easily.</p>', 'upload/8193537574.png', 'admin', 'Content Management System', '2016-11-16 18:12:57', 29),
(23, 3, 'Social Networking Solutions', '<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 500 company or a small start-up.</p>\r\n<p>&nbsp;</p>\r\n<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 100 company or a small start-up.</p>', 'upload/889de4681a.png', 'admin', 'Social Networking Solutions', '2016-11-16 18:14:24', 29),
(24, 2, 'Web Hosting', '<p><strong>Linux Hosting:</strong><br /><br />Linux web site hosting is a powerful and reliable hosting solution and as Linux is an open source software, it is affordable. Linux is best if you need MySQL support including PHP scripting<br /><br /><br /><strong>Virtual Private Server(VPS):</strong></p>\r\n<p>Are you looking for reliable yet inexpensive way to host your business application? We provides fast, reliable and secure managed web VPS hosting services.<br /><br /><strong>Corporate Shared Hosting</strong>:</p>\r\n<p>When you select one of our Corporate Web Hosting Plans, you\'re not just getting an address on the World Wide Web. You\'re getting one of the finest homes for your site on the Internet.<br /><br /><strong>Dedicated Server:</strong><br />Combining industry-leading performance, reliability and support at an affordable price are the hallmarks of our Dedicated Web Hosting Packages.</p>', 'upload/87cc9d5485.jpg', 'admin', 'Web Hosting', '2016-11-16 18:14:42', 29),
(25, 2, 'Ecommerce Solution Provider', '<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 500 company or a small start-up.</p>\r\n<p>&nbsp;</p>\r\n<p>Zsoft IT Solution Pvt.ltd &nbsp;web application development practice addresses a wide range of business needs. Whether it\'s a content management system or a web-based data interface, our solutions demonstrate all the hallmarks of competence. Our web applications address the specific business requirements, whether it\'s a fortune 500 company or a small start-up.</p>', 'upload/f4d7e79d34.png', 'admin', 'Ecommerce Solution Provider', '2016-11-16 18:14:54', 29),
(26, 3, 'Mobile App developer', '<p>Zsoft IT Solution Pvt.ltd &nbsp;specializes in developing multiplatform mobile apps for iPhone, iPad, Android. Along with developing publicly available apps via Apple App Store or Google Play, we can also develop custom business to business (b2b) apps addressing your specific business process.</p>\r\n<p>&nbsp;</p>\r\n<p>We employ cutting edge development techniques while developing your app, and strictly focus on creating a rich interface that is both attractive and intuitive. Get the best affordable web services at Zsoft IT Solution Pvt.ltd. Zsoft IT Solution Pvt.ltd &nbsp;is one of the top rated award winning android development company in Bangladesh and also familiar as the best iOS development company in BD too. Android and iOS are top used OS in Bangladesh now. As an expert android app developer company in Dhaka Bangladesh, Zsoft IT Solution Pvt.ltd &nbsp;has developed 5 apps till now.</p>\r\n<p>&nbsp;</p>\r\n<p>It&rsquo;s age of smart phone. People want to access everything via smart devices and also want to get it all done online such as buying and selling of product and services, business deal, shopping and even for groceries people use smartphone now. Every business should have mobile app to get more traffic and more sales so that more people can reach via smartphone. Statistics says that business with mobile app sells much better than a business without mobile app</p>\r\n<p>&nbsp;</p>\r\n<p>If you are a businessman and looking for the best mobile app developer to develop mobile app for your business Zsoft IT Solution Pvt.ltd &nbsp;can help you. Zsoft IT Solution Pvt.ltd has the best mobile app developer team.</p>\r\n<p>&nbsp;</p>\r\n<p>iPhone application development is another popular service of Zsoft IT Solution Pvt.ltd. This mobile app development company is expert at custom mobile applications on both android and iOS.</p>\r\n<p>&nbsp;</p>\r\n<p>If you are looking for an android mobile app developer or an android development company in Bangladesh, Zsoft IT Solution Pvt.ltd is the best choice for you</p>', 'upload/588ee73a3b.png', 'admin', 'Mobile App developer', '2016-11-16 18:15:29', 29),
(27, 3, 'Domain Hosting & Domain Registration Services ', '<p>Zsoft IT Solution Pvt.ltd, The Most Trusted Domain hosting Company in Bangladesh Since 2008</p>\r\n<p>&nbsp;</p>\r\n<p>Zsoft IT Solution Pvt.ltd &nbsp;offers the safest hosting services and domain registration services in Bangladesh. Domain is your online identity. It makes you individual from others. If you are thinking of developing website you have to think about hosting and domain registration too. Without hosting and domain you won\'t be visible online. Domain is like a name or brand to make your business recognizable &amp; individual from others. On the other hand hosting is like a place you are buying to settle your business.</p>', 'upload/1c672c14fd.png', 'admin', 'Domain Hosting & Domain Registration Services ', '2016-11-16 18:17:46', 29);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`id`, `user_id`, `name`, `username`) VALUES
(29, '', '', ''),
(30, '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `title`, `image`) VALUES
(5, 'Can you avoid knowledge? You cannot! Can you avoid technology? You cannot! ', 'upload/slider/7c3c8af1ff.jpg'),
(6, 'The empires of the future are the empires of the mind.', 'upload/slider/5a397246c7.jpg'),
(7, 'I have not failed. Iâ€™ve just found 10,000 ways that wonâ€™t work', 'upload/slider/50f2ef7a30.jpg'),
(8, 'The science of today is the technology of tomorrow.', 'upload/slider/8c1f8f2766.jpg'),
(9, 'It is only when they go wrong that machines remind you how powerful they are', 'upload/slider/24ad462f49.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_social`
--

CREATE TABLE `tbl_social` (
  `id` int(11) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `ln` varchar(100) NOT NULL,
  `gp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_social`
--

INSERT INTO `tbl_social` (`id`, `fb`, `tw`, `ln`, `gp`) VALUES
(1, 'http://facebook.com/zsoftbangladesh', 'http://twitter.com/zsoftbangladesh', 'http://linkdin.com/zsoftbangladesh', 'http://google.com/zsoftbangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_them`
--

CREATE TABLE `tbl_them` (
  `id` int(11) NOT NULL,
  `theme` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_them`
--

INSERT INTO `tbl_them` (`id`, `theme`) VALUES
(1, 'red');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `username`, `password`, `email`, `details`, `role`) VALUES
(33, 'mr. customer', 'hasan mahadi', 'e10adc3949ba59abbe56e057f20f883e', 'tomalofficial@gmail.com236', '<p>drtydrtdrtdrt</p>', 2),
(29, 'hasan mahadi (tomal)', 'admin', '202cb962ac59075b964b07152d234b70', 'tomalofficial@gmail.com', '<p><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley&nbsp;</span></p>', 0),
(30, 'sahbiha ahmmed', 'editor', '202cb962ac59075b964b07152d234b70', 'tomalofficial@gmail.com', '<p><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley&nbsp;</span></p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `title_slogan`
--

CREATE TABLE `title_slogan` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slogan` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `title_slogan`
--

INSERT INTO `title_slogan` (`id`, `title`, `slogan`, `logo`) VALUES
(1, 'Zsoft IT Solution PVT.LTD', 'Total IT solution', 'upload/logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_registration`
--

CREATE TABLE `user_registration` (
  `id` int(10) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userGender` varchar(100) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userPass` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_registration`
--

INSERT INTO `user_registration` (`id`, `userName`, `userGender`, `userEmail`, `userPass`) VALUES
(1, 'raj', 'male', 'email', 'password'),
(2, 'Raj Khan', 'Male', 'raj@gmail.com', '12345678'),
(3, 'Ekramul', 'Male', 'demo1', 'demo1'),
(4, 'lavlu', '', 'lavlu@gmail.com', '12345'),
(5, 'Ekramul lavlu', '', 'e@gmail.com', '123456789'),
(6, 'Nijhum', 'Male', 'nijhum@gmail.com', '123456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_information`
--
ALTER TABLE `customer_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suplier_information`
--
ALTER TABLE `suplier_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_footer`
--
ALTER TABLE `tbl_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_social`
--
ALTER TABLE `tbl_social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_them`
--
ALTER TABLE `tbl_them`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title_slogan`
--
ALTER TABLE `title_slogan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_registration`
--
ALTER TABLE `user_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_information`
--
ALTER TABLE `customer_information`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suplier_information`
--
ALTER TABLE `suplier_information`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_footer`
--
ALTER TABLE `tbl_footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_social`
--
ALTER TABLE `tbl_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_them`
--
ALTER TABLE `tbl_them`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `title_slogan`
--
ALTER TABLE `title_slogan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_registration`
--
ALTER TABLE `user_registration`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
