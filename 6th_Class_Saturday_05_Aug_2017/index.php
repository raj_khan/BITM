<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Class Practies</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!--Header Area Start-->
        <div class="header">
            <h1>Admin Pannel</h1>
        </div>

        <!--
        -------------------------------
        Left Sidebar Area strat
        --------------------------------
        
        -->

        <!--Single admin area-->
        <div class="admin_area">
            <div class="single_admin_area">
                <h3>Admin Area</h3>
                <ul>
                    <li id="active_menu"><a>Admin-Area</a></li>
                    <li><a>Admin Control</a></li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                </ul>


                <!--Single Entry area-->
                <h3>Entry Area</h3>
                <ul>
                    <li id="active_menu"><a>Entry-Area</a></li>
                    <li><a>Admin Control</a></li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                </ul>

                <!--Single Edit Delete area-->
                <h3>Edit / Delete</h3>
                <ul>
                    <li id="active_menu"><a>Edit-Delete</a></li>
                    <li><a>Admin Control</a></li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                    <li><a>Admin Control</a> </li>
                </ul>
            </div>
        </div>
        <!--=================================Left Sidebar Area End========================================-->



        <!--
        ----------------------------------
        Content Area Start
        ------------------------------
        -->

        <div id="content_area_strat">

            <!--Single Customer area-->
            <div class="customer_area">
                <h1>Admin Home Page</h1>
                <h3>Customer List</h3>
                <div class="customer_list_table">
                    <table class="customer_list" border="1" width="98%;" style="text-align:center;">
                        <tr style="background-color: #c6d7f3; padding: 10px 0; height: 20px;">
                            <th height="60">First Name</th>
                            <th>Last Name</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>   
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>   
                    </table>
                </div>

            </div>
            <!--End Customer area-->

            <!--Single Order list area-->
            <div class="order_list_area">
                <h3>Order List</h3>
                <table class="customer_list" border="1" width="98%;" style="text-align:center;">
                        <tr style="background-color: #c6d7f3; padding: 10px 0; height: 20px;">
                            <th height="60">First Name</th>
                            <th>Last Name</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>   
                    </table>
            </div>
            <!--End Order List area-->

            <!--Single Order list area-->
            <div class="order_list_area">
                <h3>Other List</h3>
                <table class="customer_list" border="1" width="98%;" valign="left" style="text-align:left;">
                        <tr style="background-color: #c6d7f3; padding: 10px 0; height: 20px;">
                            <th height="60">First Name</th>
                            <th>Last Name</th>
                            <th>Amount</th>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>
                        <tr>
                            <td>Raj</td>
                            <td>Khan</td>
                            <td>$1000</td>
                        </tr>   
                    </table>
            </div>
            <!--End Order List area-->




            <!--
            -------------------------------
            Right Sidebar Area strat
            --------------------------------
            
            -->


            <div class="right_sidebar">

            </div>
            <!--=================================Right Sidebar Area End========================================-->




        </div>
        <!--
          -------------------------------------------------
          Main Footer Start
          ------------------------------------------------
          
        -->
        <div class="footer_area">
            <p>Copyright &copy; 2017, BITM-PHP-61 Batch All Rights Reserved.</p>
        </div>
    </body>
</html>
