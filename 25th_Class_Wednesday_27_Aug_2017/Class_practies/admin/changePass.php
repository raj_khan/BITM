<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include("config/config.php");


include("inc/header.php");
?>
<?php
if (isset($_POST['passChangeSubmit'])) {

    $currentPass = $_POST['currentPass'];
    $newPass = $_POST['newPass'];
    $newConfirmPass = $_POST['newConfirmPass'];

    $userId = $_SESSION["s_id"];
    $query = "SELECT * FROM `admin` WHERE `id` = '$userId'";

    $checkUser = mysqli_query($con, $query);
    $userRow = mysqli_fetch_array($checkUser, MYSQLI_ASSOC);

    if (strcmp($userRow['pass'], $currentPass) == 0) {
        if (strcmp($userRow['pass'], $currentPass) == 0) {
            $updatePassQuery = "UPDATE `admin` SET `pass` = '$newPass' WHERE `admin`.`id` = $userId;";
            $updatedPassword = mysqli_query($con, $updatePassQuery);
            if ($updatedPassword) {
                $notification = "Your password is succesfully changned";
            } else {
                $notification = "Sorry your password is not changed";
            }
        } else {
            $notification = "Sorry Password and Confirm Password doesn't match";
        }
    } else {
        $notification = "Sorry your old password doesn't match";
    }
}
?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">
                        <span class="glyphicon glyphicon-bookmark"></span> Welcome To POS Admin Panel
                    </h3>
                </div>


<?php include('inc/menu.php'); ?>


                <div class="content">
                    <!--Detached content--> 
                    <div class="container-detached">

                        <!-- Grid -->
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
<?php
if (isset($notification)) {
    ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger alert-styled-left">
                                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                                <p class="text-center"><?php echo $notification; ?></p>
                                            </div>
                                        </div>
                                    </div>
    <?php
}
?>
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h3 class="text-center text-info" style="font-weight: 700;">Change Your Password</h3>
<hr>
                                    </div>

                                    <div class="panel-body">
                                        <form class="form-horizontal" method="POST" enctype="multipart/form-data">  
                                            <div class="form-group" id="add_latest_title">
                                                <label class="control-label col-lg-2">Current Password</label>
                                                <div class="col-lg-10">
                                                    <input type="password" placeholder="Please provide current password" name="currentPass" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group" id="add_latest_title">
                                                <label class="control-label col-lg-2">New Passowrd</label>
                                                <div class="col-lg-10">
                                                    <input type="password" placeholder="Please provide New password"  name="newPass" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group" id="add_latest_title">
                                                <label class="control-label col-lg-2">confirm Passowrd</label>
                                                <div class="col-lg-10">
                                                    <input type="password" placeholder="Please provide repeat password for confirm"  name="newConfirmPass" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <input name="passChangeSubmit" value="Change Password" class="btn bg-slate" type="submit">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>




                <div class="panel-body">


                    <a style="margin-top: 100px;" href="https://www.linkedin.com/in/raj-khan/" class="btn btn-primary btn-lg btn-block" role="button" target="_blank"> Copyright  &copy; 2017, Raj Khan All Right Reserved
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>










</body>
</html>
