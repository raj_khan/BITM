<?php
include '../admin/inc/header.php';
include '../admin/config/config.php';

include '../admin/inc/menu.php';
?>


<h3 class="text-center text-info" style="font-weight: 700;">Customer Information Table</h3>
<hr>


<div class="container">
    <div class="row col-md-12 custyle">
        <table class="table table-striped custab">
            <thead>
                <!--<a href="#" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new categories</a>-->
                <tr>
                    <th>Serial No.</th>
                    <th>Customer Name</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Country</th>
                    <th>Mobile No.</th>
                    <th>Email ID</th>
                    <th>Language</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>

            <?php
            $sql_menu = mysqli_query($con, "SELECT * FROM `customer_information` ORDER BY `customer_information`.`id` ASC");
            while ($data = mysqli_fetch_array($sql_menu, MYSQLI_ASSOC)) {
                ?>


                <tr>
                    <td><?php echo $data['id']; ?></td>
                    <td><?php echo $data['customerName']; ?></td>
                    <td><?php echo $data['customerGender']; ?></td>
                    <td>
                        <?php
                        $description = $data['customerAddress'];
                        $length = strlen($description);
                        if ($length > 15) {
                            $description = substr($description, 0, 8);
                            echo $description . '&nbsp; <span class=text-danger>More.....</span>';
                        } else {
                            echo $description;
                        }
                        ?>
                    </td>
                    <td><?php echo $data['customerCountry']; ?></td>
                    <td><?php echo $data['customerMobileNo']; ?></td>
                    <td><?php echo $data['customerEmail']; ?></td>
                    <td><?php echo $data['customerLanguage']; ?></td>
                    <td class="text-center">
                        <a class='btn btn-info btn-xs' href="update.php?customer_id=<?php echo $data['id']; ?>"><span class="glyphicon glyphicon-edit"></span> Edit</a> 
                        <a href="deleteProduct.php?customer_id=<?php echo $data['id']; ?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a>
                    </td>
                </tr>

            <?php } ?>

        </table>
    </div>
</div>

<?php
include '../admin/inc/footer.php';
?>
