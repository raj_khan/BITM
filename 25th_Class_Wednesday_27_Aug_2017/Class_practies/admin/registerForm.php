<?php
include 'inc/header.php';
include 'config/config.php';
?>

<?php
if (isset($_POST['submit'])) {
    $userName = $_POST['userName'];
    $userGender = $_POST['userGender'];
    $userEmail = $_POST['userEmail'];
    $userPass = $_POST['userPassword'];


  $userRegistrationQuery  = "INSERT INTO `user_registration` (`id`, `userName`, `userGender`, `userEmail`, `userPass`) VALUES(NULL, '$userName', '$userGender', '$userEmail', '$userPass')";

//
//
//    $product_insert_query = "INSERT INTO `suplier_information` (`id`, `suplierName`, `suplierGender`, `suplierAddress`, `suplierCountry`, `suplierMobileNo`, `suplierEmail`, `suplierLanguage`) VALUES(NULL, '$suplierName', '$suplierGender', '$suplierAddress', '$suplierCountry', '$suplierMobileNo', '$suplierEmail', '$suplierLanguage')";





    $insert_query = mysqli_query($con, $userRegistrationQuery);

    if ($insert_query) {

        $error = "Success! Upload your information ";
    } else {
        $error = "Error! Could not Upload your Information";
    }
}
?>


<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>








<div class="container">
    <div class="row">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center">
                    <span class="glyphicon glyphicon-bookmark"></span> Welcome To POS Admin Panel
                </h3>
            </div>



            <h3 class="text-center text-info" style="font-weight: 700;">User  Registration Form</h3>
            <hr>


            <form class="form-horizontal" action="" method="post">
                <fieldset>
                    <!-- Form Name -->

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="Name"> Full Name</label>  
                        <div class="col-md-5">
                            <input id="Name" name="userName" type="text" placeholder="User Full Name" class="form-control input-md" required="">

                        </div>
                    </div>


                    <!-- Multiple Radios (inline) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="gender">Gender</label>
                        <div class="col-md-4"> 
                            <label class="radio-inline" for="gender-0">
                                <input type="radio" name="userGender" id="gender-0" value="Male" checked="checked">
                                Male
                            </label> 
                            <label class="radio-inline" for="gender-1">
                                <input type="radio" name="userGender" id="gender-1" value="Female">
                                Female
                            </label>
                        </div>
                    </div>




                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="emailId">Email Id</label>  
                        <div class="col-md-6">
                            <input id="emailId" name="userEmail" type="text" placeholder="user@domain.com" class="form-control input-md" required="">

                        </div>
                    </div>

                    <!-- Password input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="passwordinput">Password</label>
                        <div class="col-md-5">
                            <input id="passwordinput" name="userPassword" type="password" placeholder="" class="form-control input-md" required="">
                            <span class="help-block">max 8 characters</span>
                        </div>
                    </div>



                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="submit"></label>
                        <div class="col-md-4">
                            <button id="submit" name="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label">Already Have a Account?</label>
                        <div class="col-md-4">
                            <a href="index.php"><i class="btn btn-primary">Login Here</i></a>
                        </div>
                    </div>

                </fieldset>
            </form>

            <div class="panel-body">


                <a style="margin-top: 100px;" href="https://www.linkedin.com/in/raj-khan/" class="btn btn-primary btn-lg btn-block" role="button" target="_blank"> Copyright  &copy; 2017, Raj Khan All Right Reserved
                </a>
            </div>
        </div>

    </div>
</div>

<?php include 'inc/footer.php'; ?>