<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Tranning</title>
    </head>
    <body>
        <fieldset>
            <legend style="color: red; border-color: red;">Application for training of small and cottage industry training institutes</legend>


            <form action="" id="form1" method="post">
                <input type="hidden" id="EDIT_PAGE" name="EDIT_PAGE" value="" />
                <input type="hidden" value="7" name="TRAINING_CENTER_TYPE" id="TRAINING_CENTER_TYPE" />
                <input type="hidden" id="validUserId" name="validUserId" value="0" />
                <input type="hidden" id="TRAINING_ID" name="TRAINING_ID" value="" />
                <input type="hidden" id="selectedShcedule" name="selectedShcedule" value="" />

                <table width="75%" align="center" style="font-size: 14px;">
                    <tr id="OFFICE_DIV">
                        <td class="labelcell" style="font-size:14px;" width="40%">Faculty name<span style="color:red">*</span></td>
                        <td class="fieldcell" width="60%" align="left">
                            <select name="OFFICE_ID" class="required" id="OFFICE_ID">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="SCITI">SCITI</option>
                            </select>    
                        </td>
                    </tr>
                    <tr id="FACULTY_DIV"> 
                        <td class="labelcell" style="font-size:14px;" width="40%">Faculty name <span style="color:red">*</span></td>
                        <td class="fieldcell" width="60%" align="left">
                            <select name="FACULTY" class="required" id="FACULTY">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="EDF">Entrepreneurship Development Faculty</option>
                                <option value="GMF">General Management Faculty</option>
                                <option value="IMF">Industrial Management Faculty</option>
                                <option value="FMF">Financial Management Faculty</option>
                                <option value="MMF">Marketing Management Faculty</option>
                                <option value="RCF">Research and Consultancy Faculty</option>
                            </select>    
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Course Name <font color="#FF0000">*</font></td>
                        <td  class="fieldcell" align="left">     	<select name="COURSE" class="required" id="COURSE">
                                <option value="" selected="selected">Select From Here</option>
                            </select>        
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Duration  <span style="color:red">*</span></td>    
                        <td class="fieldcell" id="session_div">&nbsp;</td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Applicant's name (in English)  <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_NAME_EN" type="text" name="APPLICANT_NAME_EN" value=""   class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Applicant's name (in Bangla)  <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_NAME" type="text" name="APPLICANT_NAME" value=""  class="required" />
                        </td>
                    </tr>

                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Father's Name (English) <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_F_NAME_EN" type="text" name="APPLICANT_F_NAME_EN" value=""  class="required" />
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;"> Father's Name (Bangla) <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_F_NAME" type="text" name="APPLICANT_F_NAME" value=""  class="required" />
                        </td>
                    </tr>

                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Mother's name (English) <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_M_NAME_EN" type="text" name="APPLICANT_M_NAME_EN" value=""  class="required" />

                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Mother's name (Bangla) <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="APPLICANT_M_NAME" type="text" name="APPLICANT_M_NAME" value=""  class="required" />
                        </td>
                    </tr>

                    <tr>
                        <td class="labelcell"  style="font-size:14px;" align="left">Permanent address</td>
                        <td class="fieldcell" align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="labelcell" style="font-size: 14px;">Division<font color="#FF0000">*</font></td>
                        <td>
                            <select name="PERM_DIVISION_ID" class="abc required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1121">Khulna</option>
                                <option value="1120">chittagong</option>
                                <option value="1119">Dhaka</option>
                                <option value="1123">Barishal</option>
                                <option value="1125">Rangpur</option>
                                <option value="1122">Rajshahi</option>
                                <option value="1124">sylet</option>
                            </select>    
                        </td>
                    </tr>

                    <tr>
                        <td class="labelcell" style="font-size:14px">District<font color="#FF0000">*</font></td>

                        <td class="fieldcell">
                            <select name="PERM_DISTRICT_ID" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1148">cozbazar</option>
                                <option value="1131">Kishorgong</option>
                                <option value="1184">Kurigram</option>
                                <option value="1147">Kummillah</option>
                                <option value="1159">Kustia</option>
                                <option value="1150">Khagrachorhi</option>
                                <option value="1158">Khulna</option>
                                <option value="1183">Gaibandha</option>
                                <option value="1128">Gazipur</option>
                                <option value="1129">Gopalgong</option>
                                <option value="1146">chittagong</option>
                            </select>            
                        </td>
                    </tr>

                    <tr>
                        <td class="labelcell" style="font-size:14px">Thana / upazila<font color="#FF0000">*</font></td>
                        <td class="fieldcell">
                            <select name="PERM_AREA_ID" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1542">Aditmari</option>
                                <option value="1203">Avainagar</option>
                                <option value="1381">astagar</option>
                                <option value="1583">Akelpur</option>
                                <option value="1256">Akharapur</option>
                                <option value="1477">Agoljhara</option>
                                <option value="1637">Ajmarigoang</option>
                                <option value="1553">Atorari</option>
                                <option value="1610">Atghria</option>
                                <option value="1434">atpara</option>
                                <option value="1423">arhaihazar</option>

                            </select>        </td>
                    </tr>
                    <tr>
                        <td class="labelcell"  style="font-size:14px;" align="left">
                            <span class="labelcell" style="font-size:14px;"> Address (Village / Mahalla / Road / Holding No.)<span style="color:red">*</span></span>
                        </td>
                        <td class="fieldcell" align="left">
                            <textarea id="PERMANENT_ADDR" name="PERMANENT_ADDR" class="required"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="labelcell"  style="font-size:14px;" align="left">Present Address</td>    
                        <td class="fieldcell" align="left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="labelcell" style="font-size: 14px;">Divition<font color="#FF0000">*</font></td>
                        <td>
                            <select name="PRESENT_DIVISION_ID" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>

                                <option value="1121">Khulna</option>
                                <option value="1120">chittagong</option>
                                <option value="1119">Dhaka</option>
                                <option value="1123">Barishal</option>
                                <option value="1125">Rangpur</option>
                                <option value="1122">Rajshahi</option>
                                <option value="1124">sylet</option>
                            </select>    
                        </td>
                    </tr>

                    <tr>
                        <td class="labelcell" style="font-size:14px">District<font color="#FF0000">*</font></td>

                        <td class="fieldcell">
                            <select name="PRESENT_DISTRICT_ID" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1148">cozbazar</option>
                                <option value="1131">Kishorgong</option>
                                <option value="1184">Kurigram</option>
                                <option value="1147">Kummillah</option>
                                <option value="1159">Kustia</option>
                                <option value="1150">Khagrachorhi</option>
                                <option value="1130">Jamalpur</option>
                                <option value="1175">Jhalkhati</option>
                                <option value="1157">Jhenaidah</option>
                        </td>
                    </tr>

                    <tr>
                        <td class="labelcell" style="font-size:14px">Thana-Upazala<font color="#FF0000">*</font></td>
                        <td class="fieldcell">
                            <select name="PRESENT_AREA_ID" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1542">Aditmari</option>
                                <option value="1203">Avainagar</option>
                                <option value="1381">astagar</option>
                                <option value="1583">Akelpur</option>
                                <option value="1256">Akharapur</option>
                                <option value="1477">Agoljhara</option>
                                <option value="1637">Ajmarigoang</option>
                                <option value="1553">Atorari</option>
                                <option value="1610">Atghria</option>
                                <option value="1434">atpara</option>
                                <option value="1423">arhaihazar</option>
                                <option value="1588">ati</option>
                                <option value="1571">adomji</option>
                                <option value="1296">Adorshodar</option>
                            </select>            
                        </td>
                    </tr>
                    <tr>
                        <td class="labelcell" style="font-size:14px;">
                            <span class="labelcell" style="font-size:14px;"> Address (Village / Mahalla / Road / Holding No.)</span>
                        </td>
                        <td class="fieldcell" align="left">
                            <textarea id="PRESENT_ADDR" name="PRESENT_ADDR"></textarea>

                        </td>
                    </tr>


                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">NID No.</td>
                        <td  class="fieldcell" align="left">
                            <input id="NATIONAL_ID_CARD_NUMBER" type="text" name="NATIONAL_ID_CARD_NUMBER" value=""  class="" />
                        </td>
                    </tr>

                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Mobile No. <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="MOBILE_NUMBER" type="text" name="MOBILE_NUMBER" value="" class="required"  />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">E-Mail <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="EMAIL" type="text" name="EMAIL" value="" class="required email" />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Sex <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <select name="SEX" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1">Male</option>
                                <option value="2">female</option>
                            </select>            
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Date of Birth <span style="color:red">*</span></td>
                        <td  class="fieldcell6" align="left"><span class="labelcell" style="font-size: 14px;">Date </span>
                            <select name="BIRTH_DAY" class="required selectbox-2">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1">01</option>
                                <option value="2">02</option>
                                <option value="3">03</option>
                                <option value="4">04</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>                
                            <span class="labelcell" style="font-size: 14px;">  Month </span>
                            <select name="BIRTH_MONTH" class="required selectbox-2">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1">01</option>
                                <option value="2">02</option>
                                <option value="3">03</option>
                                <option value="4">04</option>
                                <option value="5">05</option>
                                <option value="6">06</option>
                                <option value="7">07</option>
                                <option value="8">08</option>
                                <option value="9">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>                
                            <span class="labelcell" style="font-size: 14px;"> Year </span>
                            <select name="BIRTH_YEAR" class="required selectbox-2">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="2017">2017</option>
                                <option value="2016">2016</option>
                                <option value="2015">2015</option>
                                <option value="2014">2014</option>
                                <option value="2013">2013</option>
                                <option value="2012">2012</option>
                                <option value="2011">2011</option>
                                <option value="2010">2010</option>
                                <option value="2009">2009</option>
                                <option value="2008">2008</option>
                                <option value="2007">2007</option>
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>

                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Married status <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <select name="MARITAL_STATUS" class="required selectbox-1">
                                <option value="" selected="selected">Select From Here</option>
                                <option value="1">Married</option>
                                <option value="2">Unmarried</option>
                            </select>            
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Nationality</td>
                        <td  class="fieldcell" align="left">
                            <input id="NATIONALITY" type="text" name="NATIONALITY" value=""  />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Current occupation <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="CURRENT_OCCUPATION" type="text" name="CURRENT_OCCUPATION" value="" class="required"  />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Education Qualification <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left">
                            <input id="EDUCATIONAL_QUALITY" type="text" name="EDUCATIONAL_QUALITY" value=""  class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Previous training details (if any)</td>
                        <td  class="fieldcell" align="left">
                            <textarea id="PREV_TRAINING" type="text" name="PREV_TRAINING" ></textarea> 
                    </tr>
                    <tr>
                        <td  class="labelcell" style="font-size: 14px;">Real experience</td>
                        <td  class="fieldcell" align="left">
                            <textarea id="REAL_EXPERIENCE" name="REAL_EXPERIENCE"></textarea>    
                        </td>
                    </tr>


                    <tr>
                        <td align="left" class="labelcell"  style="font-size: 14px;">Login ID  <span style="color:red">*</span></td>
                        <td class="fieldcell"><input type="text" name="loginId" id="loginId" value="" class="required" />
                            (Minimum 5 dizit)
                            <br />
                            <span id="result" style="color:#900">
                                <input type="hidden" name="loginIdCheck" id="loginIdCheck" value="-1" />
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td align="left"  class="labelcell"  style="font-size: 14px;">Password <span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left"><input type="password" name="new" id="new" class="" /> (Minimunm five dizit)</td>
                    </tr>
                    <tr>
                        <td align="left"  class="labelcell"  style="font-size: 14px;">Reset password<span style="color:red">*</span></td>
                        <td  class="fieldcell" align="left"><input type="password" name="new_repeat" id="new_repeat" /></td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>

                <input type="hidden" value="" id="img_name_with_path" name="img_name_with_path" class="required" />
                <input type="hidden" value="" id="sig_name_with_path" name="sig_name_with_path" class="required" />         



            </form>



            <table width="75%" align="center" style="font-size: 14px;">
                <tr>
                    <td class="labelcell" style="font-size:14px;" width="40%">Upload your Image <span style="color:red">*</span></td>
                    <td class="fieldcell" width="60%" align="left">
                        <form id="form2" name="form2" method="post">    
                            <input type="file" name="photo_img" id="photo_img" class="" />
                            <div id="preview">
                                <input type="hidden" value="" id="img_name_with_path2" name="img_name_with_path2" />
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td class="labelcell" style="font-size:14px;" width="40%">upload your signeture <span style="color:red">*</span></td>
                    <td class="fieldcell" width="60%" align="left">
                        <form id="form3" name="form3" method="post">    
                            <input type="file" name="photo_sig" id="photo_sig" class="" />
                            <div id="preview2">
                                <input type="hidden" value="" id="sig_name_with_path2" name="sig_name_with_path2" />
                            </div>
                        </form>
                    </td>
                </tr>
                <tr align="center">
                    <td class="fieldcell" colspan="2" style="color:red; font-size:16; font-weight:bold" id="invalidInst"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="button" name="submit2" id="submit2" value="Submit" class="btn-submit-1" /></td>
                </tr>
            </table>
        </fieldset>

    </div>
</body>
</html>
