-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2017 at 07:29 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apl`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(55) NOT NULL,
  `pass` varchar(55) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `pass`, `status`) VALUES
(1, 'apl@gmail.com', '123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category_id_name`
--

CREATE TABLE `category_id_name` (
  `catagory_id` int(11) NOT NULL,
  `catagory_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_id_name`
--

INSERT INTO `category_id_name` (`catagory_id`, `catagory_name`) VALUES
(1, 'Main Slider'),
(2, 'Feature Product'),
(3, 'Product'),
(4, 'Gallery'),
(5, 'Welcome Message'),
(6, 'About Us'),
(7, 'Global Partner');

-- --------------------------------------------------------

--
-- Table structure for table `slider_table`
--

CREATE TABLE `slider_table` (
  `id` int(55) NOT NULL,
  `category` int(55) NOT NULL,
  `title` text NOT NULL,
  `img_link` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_table`
--

INSERT INTO `slider_table` (`id`, `category`, `title`, `img_link`, `description`) VALUES
(1, 1, 'Welcome to APL Bangladesh', '593cd3ecb4070rahat2.jpg', ''),
(2, 1, 'Welcome to APL Bangladesh', '593cd59a64558rahat3.jpg', ''),
(3, 1, 'Welcome to APL Bangladesh', '593cd5b6ee2f1rahat1.jpg', ''),
(4, 1, 'Welcome to APL Bangladesh', '593cd5bd5764eBagda.jpg', ''),
(5, 1, 'Welcome to APL Bangladesh', '593cd5c3dd465rahat4.jpg', ''),
(6, 1, 'Welcome to APL Bangladesh', '593cd5ca96145rahat5.jpg', ''),
(7, 5, '', '', 'We are very selective about the type of products that we import because \r\nthe end users/consumers depends on it. That is why we always sell \r\nproduct who admire for quality assurance.<br>\r\n01. Safety<br>\r\n02. Effectiveness<br>\r\n03. Productivity<br>\r\nAbsolute Transparency toward our customers. <br>'),
(8, 2, 'Passion Fruits', '593cd757915e7product-1.jpg', ''),
(9, 2, 'Kiwi Fruits', '593cd76712634product-2.JPG', ''),
(10, 2, 'Tomatoes', '593cd772a48bbproduct-3.jpg', ''),
(11, 2, 'Ripe pea', '593cd77d60179product-4.jpg', ''),
(13, 2, 'Rye Bread', '593cd7ce63aa2product-6.jpg', ''),
(14, 2, 'Figs Sweet', '593cd85da3ebdproduct-5.jpg', ''),
(15, 3, 'Figs Sweet', '593cd9ef05b51product-1.jpg', ''),
(16, 3, 'Figs Sweet', '593cd9f7264aaproduct-2.JPG', ''),
(17, 3, 'Figs Sweet', '593cd9fd8171eproduct-3.jpg', ''),
(18, 3, 'Product Name', '593cda08bced9product-4.jpg', ''),
(19, 3, 'Product Name', '593cda129600cproduct-5.jpg', ''),
(20, 3, 'Product Name', '593cda19a9251product-6.jpg', ''),
(21, 4, 'Gallery', '593cdaf8cb82cgallery-1.jpg', ''),
(22, 4, 'Gallery', '593cdb00537fagallery-2.jpg', ''),
(23, 4, 'Gallery', '593cdb0667d8dgallery-3.jpg', ''),
(24, 4, 'Gallery', '593cdb0ba89e3gallery-4.jpg', ''),
(25, 4, 'Gallery', '593cdb112af64gallery-5.jpg', ''),
(26, 4, 'Gallery', '593cdb16f0cc1gallery-6.jpg', ''),
(27, 4, 'Gallery', '593cdb1c7844cgallery-7.jpg', ''),
(28, 4, 'Gallery', '593cdb223a17cgallery-8.jpg', ''),
(29, 6, '', '', 'We are specialized for import and distribution of feed ingredients and \r\nadditives for poultry fish and dairy.\r\n                                    Over time, APL have proved as a \r\nreliable and committed supplier to Agricultural Industries.\r\n                                    Our professionals experience and \r\nresources allows us to offer the best products for animal feeding with \r\nthe best our availability.\r\n                                    \"Grow Together\" Is Our Main \r\nTheme.Commitment quality assurance from well reputed prestigious company\r\n around the globe.<br>'),
(30, 7, 'partner', '593cded569be5mercordi.png', ''),
(31, 7, 'partner', '593cdfa61291ammb34.jpg', ''),
(32, 7, 'partner', '593cdfac14d9anutiall.png', ''),
(33, 7, 'partner', '593cdfb187eabnutriad.png', ''),
(34, 7, 'partner', '593cdfb6b69ecSuba Nutrients Logo.jpg', ''),
(35, 7, 'partner', '593cdfbccf5d1sunnynutri.png', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_id_name`
--
ALTER TABLE `category_id_name`
  ADD PRIMARY KEY (`catagory_id`);

--
-- Indexes for table `slider_table`
--
ALTER TABLE `slider_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category_id_name`
--
ALTER TABLE `category_id_name`
  MODIFY `catagory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `slider_table`
--
ALTER TABLE `slider_table`
  MODIFY `id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
