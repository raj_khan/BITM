
<?php include 'inc/header.php';
include_once './model/slider.php';
$sliderObject = new slider();
$showAll = $sliderObject->showPartner();
?>    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/rahat1.jpg);">
        <div class="auto-container">
            <h1>Global Partner</h1>
        </div>
    </section>
    

    
    
    <!--sponsors style one-->
    <section class="sponsors-style-one">
    	<div class="auto-container">
        	<!--Section Title-->
            <div class="sec-title-one">
                <h2>Our partners</h2>
            </div>
            
    		<div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel">
                    <?php
                foreach ($showAll as $Data) {
                    ?>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="img/all-images/<?php echo isset($Data['img_link']) ? $Data['img_link'] : null ?>" alt=""></a></figure></li>
                    <?php } ?>
<!--                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/partner/mmb34.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/partner/nutiall.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/partner/nutriad.png" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/partner/Suba Nutrients Logo.jpg" alt=""></a></figure></li>
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="images/partner/sunnynutri.png" alt=""></a></figure></li>-->
                </ul>
            </div>
        </div>
    </section>
<?php include 'inc/footer.php'; ?>    
