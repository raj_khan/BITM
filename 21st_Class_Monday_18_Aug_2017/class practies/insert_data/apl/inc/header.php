<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>APL Bangladesh</title>
        <!-- 
        -----------------------------------
        Style sheets 
        -----------------------------------
        1. bootstrap
        2. revolution-slider
        3. style
        4. responsive
        -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/revolution-slider.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
        <link rel="icon" href="images/favicon.png" type="image/x-icon">
        <!-- Responsive -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
        <!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
    </head>

    <body>

        <div class="page-wrapper">

            <!--
            ------------------------------------------
            Pre loader 
            ------------------------------------------
            1. round Shape Pre Loader
            -->
            <div class="preloader"></div>



            <!--
            --------------------------------------------
            Main Header
            --------------------------------------------
            1. Main Box
            2. logo
            3. Nav Outer
            4. Mobile Menu
            -->
            <header class="main-header">

                <!-- Main Box -->
                <div class="main-box">
                    <div class="auto-container">
                        <div class="outer-container clearfix">

                            <!--Logo Box-->
                            <div class="logo-box logo-img">
                                <div class="logo">
                                    <a href="index.php">
                                        <img src="images/logo.png" alt="">
                                    </a>
                                </div>
                            </div>

                            <!--Nav Outer-->
                            <div class="nav-outer clearfix">
                                <!-- Main Menu -->
                                <nav class="main-menu">
                                    <div class="navbar-header">
                                        <!-- Toggle Button -->    	
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div class="navbar-collapse collapse clearfix">
                                        <ul class="navigation clearfix">
                                            <li class="current"><a href="index.php">Home</a></li>
                                            <li><a href="about-us.php">About Us</a></li>
                                            <li><a href="product.php">Product</a></li>
                                            <li><a href="gallery.php">Gallery</a></li>
                                            <li><a href="partner.php">Global Partner</a></li>
                                            <li><a href="contact.php">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </nav><!-- Main Menu End-->

                            </div><!--Nav Outer End-->

                            <!-- Hidden Nav Toggler -->
                            <div class="nav-toggler">
                                <button class="hidden-bar-opener"><span class="icon fa fa-bars"></span></button>
                            </div><!-- / Hidden Nav Toggler -->

                        </div>    
                    </div>
                </div>

            </header>
            <!--End Main Header -->


            <!--
            --------------------------------------
            Hidden Navigation Bar
            --------------------------------------
            
            -->
            <section class="hidden-bar right-align">

                <div class="hidden-bar-closer">
                    <button class="btn"><i class="fa fa-close"></i></button>
                </div>

                <!-- Hidden Bar Wrapper -->
                <div class="hidden-bar-wrapper">

                    <!-- .logo -->
                    <div class="logo text-center">
                        <a href="index.php"><img src="images/logo.png" alt=""></a>			
                    </div><!-- /.logo -->

                    <!-- .Side-menu -->
                    <div class="side-menu">
                        <!-- .navigation -->
                        <ul class="navigation">
                            <li class="current"><a href="index.php">Home</a></li>
                            <li><a href="about-us.php">About Us</a></li>
                            <li><a href="product.php">Product</a></li>
                            <li><a href="gallery.php">Gallery</a></li>
                            <li><a href="partner.php">Global Partner</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                        </ul>
                    </div><!-- /.Side-menu -->

                    <div class="social-icons">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>

                </div><!-- / Hidden Bar Wrapper -->
            </section>
            <!-- / Hidden Bar -->