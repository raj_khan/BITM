<?php

include_once("Database.php");

class TeamMemberClass extends Database{
    
    public $con;

    public function __construct() {
        
        $this->con = parent::connect();
    }
    public function insert($postData) {
        
        $prod_catagory = $postData['prod_catogory'];
        $description = $postData['discription'];
        
        $description = mysqli_real_escape_string($this->con, $description);
        

        $product_insert_query = "INSERT INTO `slider_table` (`id`, `category`, `title`, `img_link`, `description`) VALUES (NULL, '$prod_catagory', '', '', '$description')";

        $insert_query = mysqli_query($this->con, $product_insert_query);
        if ($insert_query) {

            return "Success! Upload your information ";	
        } else {
            return "Error! Could not Upload your Information";
        }
    }
    public function showWelcomeText() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 5 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
    public function showMission() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 6 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
    public function showVision() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 10 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
    public function showBest() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 11 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
    public function showWhyDifferent() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 12 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
    public function showSubcidary() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 13 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);      
           $returnData = $row;
          return $returnData;    
    }
   
    public function showForEditTeam() {
        
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 5 OR `category` = 6 ORDER BY `slider_table`.`id` ASC ';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
            
        }

        return $returnData;
    }
        public function showTeamCategory($cat_id) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $cat_id . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
             $showCategoryName= mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            return $showCategoryName['catagory_name'];
        
    }
    public function showSingleTeam($id) {
        
         $queryForSingleTeam = "SELECT * FROM `slider_table` WHERE `id` = '$id'";
        $SingleTeam = mysqli_query($this->con, $queryForSingleTeam);
        $returnData = mysqli_fetch_array($SingleTeam, MYSQLI_ASSOC);

        return $returnData;   
        
        
    }
    public function updateTeam($postData, $fileData) {
        
        $prod_catagory = $postData['prod_catogory'];
        $description = $postData['discription'];
        $id = $postData['id'];
         
        $description = mysqli_real_escape_string($this->con, $description);
        
        $singleTeamMember = $this->showSingleTeam($id);
        
        if ($prod_catagory == "") {
            $error = "Please input Category";
        }else if ($description == "") {
            $error = "Please input Description";
        }
        
        $product_update_query = "UPDATE `slider_table` SET`category` = '$prod_catagory', `title` = '', `img_link` = '', `description` = '$description' WHERE `slider_table`.`id` = $id";
        
        
        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "viewTeamMember.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }
    public function getAllCats(){
            $returnData = '';
            $query  = "SELECT * FROM `category_id_name`";
            $SingleCat = mysqli_query($this->con, $query);
            while ($row = mysqli_fetch_array($SingleCat, MYSQLI_ASSOC)){
                $returnData[] = $row;
            }
            

            return $returnData; 
    }
}
