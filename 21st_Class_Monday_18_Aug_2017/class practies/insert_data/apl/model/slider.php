<?php

include_once("Database.php");

class slider extends Database {

    public $con;

    public function __construct() {

        $this->con = parent::connect();
    }

    public function insert($postData, $fileData) {

        $prod_catagory = $postData['prod_catogory'];
        $prod_title = $postData['add_title'];

        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../img/all-images/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `slider_table` (`id`, `category`, `title`, `img_link`, `description`) VALUES (NULL, '$prod_catagory', '$prod_title', '$file_name', '')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }
    public function insertAbout($postData, $fileData) {

        $prod_catagory = $postData['prod_catogory'];
        $prod_des = $postData['discription'];

        $prod_des = mysqli_real_escape_string($this->con, $prod_des);

        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../img/all-images/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `slider_table` (`id`, `category`, `title`, `img_link`, `description`) VALUES (NULL, '$prod_catagory', '', '$file_name', '$prod_des')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }

    public function showSliderImages() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 1  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 4) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    
    public function showFeaturedProduct() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 2  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 11) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
     public function showCompleteProjectImages() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 3 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 31) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    
    public function showUpcomingProduct() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 4 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 31) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    public function showPartner() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 7 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 31) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
        public function showUpcomingProj() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 6 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 31) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
        public function showGallery() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 7 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 31) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
   
    public function showdirectorPannelImage() {

        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 14 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);
        $returnData = $row;
        
        return $returnData;
    }
    public function showhanaImage() {

        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 15 ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

       $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);
        $returnData = $row;
        
        return $returnData;
    }
    
    

    public function showForEdit() {

        $sql_query = 'SELECT * FROM `slider_table` WHERE category < 5 OR category = 7 ORDER BY `slider_table`.`category` ASC';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }
    public function showForEditAbout() {

        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` > 13';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }

    public function showSliderCategory() {

        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `slider_table`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row_for_category = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $row_for_category['category'] . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
            $showCategoryName = mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            $returnData = $showCategoryName;
        }
        return $returnData;
    }
    public function showForcat() {
        
        $sql_query = 'SELECT * FROM `slider_table` ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
            
        }

        return $returnData;
    }
    public function showCatName($cat_id) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $cat_id . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
             $showCategoryName= mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            return $showCategoryName['catagory_name'];
        
    }

    public function showCategory() {
        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `category_id_name`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }
        return $returnData;
    }

    public function showSingleSlider($id) {

        $queryForSingleSlider = "SELECT * FROM `slider_table` WHERE `id` = $id";
        $SingleSlider = mysqli_query($this->con, $queryForSingleSlider);
        $returnData = mysqli_fetch_array($SingleSlider, MYSQLI_ASSOC);

        return $returnData;
    }

    public function updateSlider($postData, $fileData) {
        
        
        $prod_catagory = $postData['prod_catogory'];
        $prod_title = $postData['add_title'];
        $id = $postData['id'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);
        
        $singleSlider = $this->showSingleSlider($id);
        
        if ($prod_title == "") {
            $error = "Please input Image Title";
        }if ($prod_catagory == "") {
            $error = "Please input Product Category";
        } else if ($fileData['file']['size'] > 250000) {
            $error = "Please insert image not more than 250 KB";
         }
        
        if (isset($fileData['file']['name']) && !empty($fileData['file']['name'])) {
            $file_name = uniqid() . $fileData['file']['name'];
            $destination = "../img/all-images/" . $file_name;
            $upload = move_uploaded_file($fileData['file']['tmp_name'], $destination);
        } else {
            $file_name = $singleSlider['img_link'];
        }
        
        $product_update_query = "UPDATE `slider_table` SET `category` = '$prod_catagory', `title` = '$prod_title', `img_link` = '$file_name', `description` = '' WHERE `slider_table`.`id` = $id";
        
        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "viewSlider.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }
    public function updateAbout($postData, $fileData) {
        
        
        $prod_catagory = $postData['prod_catogory'];
        $prod_des = $postData['discription'];
        $id = $postData['id'];
        $prod_des = mysqli_real_escape_string($this->con, $prod_des);
        
        $singleSlider = $this->showSingleSlider($id);
        
        if ($prod_des == "") {
            $error = "Please input Image Description";
        }if ($prod_catagory == "") {
            $error = "Please input Product Category";
        } else if ($fileData['file']['size'] > 250000) {
            $error = "Please insert image not more than 250 KB";
         }
        
        if (isset($fileData['file']['name']) && !empty($fileData['file']['name'])) {
            $file_name = uniqid() . $fileData['file']['name'];
            $destination = "../img/all-images/" . $file_name;
            $upload = move_uploaded_file($fileData['file']['tmp_name'], $destination);
        } else {
            $file_name = $singleSlider['img_link'];
        }
        
        $product_update_query = "UPDATE `slider_table` SET `category` = '$prod_catagory', `title` = '', `img_link` = '$file_name', `description` = '$prod_des' WHERE `slider_table`.`id` = $id";
        
        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "viewAboutImage.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }
    
    
    
    
    
    
    public function showHomeApartments() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 2  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 14) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    
    public function showAllBlock() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 3  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 14) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    public function showLandProject() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 7  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 14) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    public function showApartmentProject() {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 8  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 14) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    public function getAllCats(){
            $returnData = '';
            $query  = "SELECT * FROM `category_id_name`";
            $SingleCat = mysqli_query($this->con, $query);
            while ($row = mysqli_fetch_array($SingleCat, MYSQLI_ASSOC)){
                $returnData[] = $row;
            }
            

            return $returnData; 
    }
   
}
