<?php

include_once("Database.php");

class NoticeClass extends Database{

    public $con;

    public function __construct() {
        
        $this->con = parent::connect();
    }

    public function insert($postData) {

        $description = $postData['description'];
      
        $description = mysqli_real_escape_string($this->con, $description);

        $product_insert_query = "INSERT INTO `notice` (`id`, `category`, `description`) VALUES (NULL, '2', '$description')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }
    
    public function ShowNotice() {
        
        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 2 ORDER BY `notice`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if($i > 2){
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    public function showForEdit() {
        
        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 2';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
            
        }

        return $returnData;
    }
    public function showSingleNotice($id) {

        $queryForSingleSlider = "SELECT * FROM `notice` WHERE `id` = $id";
        $SingleSlider = mysqli_query($this->con, $queryForSingleSlider);
        $returnData = mysqli_fetch_array($SingleSlider, MYSQLI_ASSOC);

        return $returnData;
    }
    public function updateNotice($postData) {

        $description = $postData['description'];
        $id = $postData['id'];
        $prod_title = mysqli_real_escape_string($this->con, $description);
        
        /**=======For Images=======**/
//        $singleNotice = $this->showSingleNotice($id);
        
        if ($description == "") {
            $error = "Please input Notice";
        }
        
        $product_update_query = "UPDATE `notice` SET `description` = '$description' WHERE `notice`.`id` = $id";
        
        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "viewNotice.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }

}
