<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include("header.php");
include_once '../model/slider.php';
$sliderObjectEdit = new slider();

$editAllSlider = $sliderObjectEdit->showForEdit();
include('headerMenu.php');
?>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 style="padding:5px" class="panel-title bg-slate">All Images :</h5>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">Category</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>	 	 	 	
                        <?php foreach ($editAllSlider as $editAllSliderImage){ ?>
                        <tr>
                            <td class="text-center">
                                <?php echo $sliderObjectEdit->showCatName($editAllSliderImage['category'])?>
                            </td>
                            <td class="text-center">
                                <?php echo isset($editAllSliderImage['title']) ? $editAllSliderImage['title'] : null ?>
                            </td> 
                            <td class="text-center">
                                <img style="width:40px; height:auto;" src="../img/all-images/<?php echo isset($editAllSliderImage['img_link']) ? $editAllSliderImage['img_link'] : null ?>">
                            </td>
                            <td class="text-center">
                                <a href="editSlider.php?slider_id=<?php echo $editAllSliderImage['id']; ?>" class="label label-primary">Edit</a>
                                <a href="deleteProduct.php?slider_id=<?php echo $editAllSliderImage['id']; ?>" class="label label-primary" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




<?php include('footer.php'); ?>
