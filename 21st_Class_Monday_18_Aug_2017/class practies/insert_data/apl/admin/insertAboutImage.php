<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
/* ========= Include All Class ========== */
include("../model/slider.php");


/* ========= Declear Object ========== */
$aboutObject = new slider();


include("header.php");
include('headerMenu.php');

if (isset($_POST['prod_upload'])) {
    $error = $aboutObject->insertAbout($_POST, $_FILES);
}
?>
<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>

<!-- Grid -->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- Horizontal form -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 style="padding:0 5px;"class="bg-grey-700 panel-title">Please insert Text & Image</h5>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" method="POST"  enctype="multipart/form-data">


                    <div class="form-group">
                        <label class="control-label col-lg-2"> Category </label>
                        <div class="col-lg-10">
                            <select name="prod_catogory" class="form-control">
                                <option></option>
                                <option value="14">Director Pannel</option>
                                <option value="15">Hana Profile</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">About Text Here </label>
                        <div class="col-lg-10">
                            <textarea cols="10" rows="10" name = "discription" class="wysihtml5 wysihtml5-default form-control" placeholder="Enter text ..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2"> Image</label>
                        <div class="col-lg-10">
                            <input type="file" name="file" class="file-styled">
                        </div>
                    </div>
                    <div class="text-right">
                        <input type="submit" name="prod_upload" value="Upload Product"class="btn bg-grey">

                    </div>
                </form>
            </div>
        </div>
        <!-- /horizotal form -->

    </div>
</div>
<script>
    $(document).ready(function () {
        $('select[name = "prod_catogory"]').select2();
    });
</script>

<?php include('footer.php'); ?>