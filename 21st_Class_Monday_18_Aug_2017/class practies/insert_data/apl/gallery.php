
<?php
include 'inc/header.php';
include_once './model/slider.php';

$sliderObject = new slider();

$showAllUpcomingProdImage = $sliderObject->showUpcomingProduct();
?>    

<!--Page Title-->
<section class="page-title" style="background-image:url(images/background/rahat2.jpg);">
    <div class="auto-container">
        <h1>Gallery</h1>
    </div>
</section>

<!--Gallery Section-->
<section class="gallery-section gallery-page">
    <div class="container-fluid">

        <div class="mixitup-gallery">

            <!--Filter List-->
            <div class="filter-list row clearfix">

                <?php
                foreach ($showAllUpcomingProdImage as $data) {
                    ?>
                    <!--Default Portfolio Item -->
                    <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box gallery-img"><img src="img/all-images/<?php echo isset($data['img_link']) ? $data['img_link'] : null ?>" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="img/all-images/<?php echo isset($data['img_link']) ? $data['img_link'] : null ?>" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php } ?>
                <!--Default Portfolio Item -->
<!--                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-2.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-2.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-3.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-3.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-4.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-4.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-5.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-5.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-6.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-6.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-7.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-7.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                Default Portfolio Item 
                <div class="default-portfolio-item mix mix_all col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box gallery-img"><img src="images/gallery/gallery-8.jpg" alt=""></figure>
                        <div class="overlay-box">
                            <div class="overlay-inner">
                                <div class="overlay-content">
                                    <a href="images/gallery/gallery-8.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                    <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->

            </div>

        </div>

    </div>
</section>

<?php include 'inc/footer.php'; ?>