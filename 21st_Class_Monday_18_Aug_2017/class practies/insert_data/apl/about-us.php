
<?php include 'inc/header.php';
include_once './model/TeamMemberClass.php';
$TextObject = new TeamMemberClass();
$missionText = $TextObject->showMission();
?>    

<!--Page Title-->
<section class="page-title" style="background-image:url(images/background/rahat4.jpg);">
    <div class="auto-container">
        <h1>About Us</h1>
    </div>
</section>


<!--About Farm Section-->
<section class="about-farm style-two">
    <div class="auto-container">
        <!--Section Title-->
        <div class="sec-title-one">
            <h2>About Us</h2>
        </div>

        <div class="row clearfix">
            <!--Column-->
            <div class="column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tabs-box tabs-style-one">
                    <!--Tabs Content-->
                    <div class="tabs-content">

                        <!--Tab-->
                        <div class="tab" id="tab-one">
                            <div class="subtitle">About Us</div>
                        </div>

                        <!--Tab / Active Tab-->
                        <div class="tab active-tab" id="tab-two">
                            <div class="subtitle">Who We Are</div>
                            <div class="content">
                                <p><?php echo $missionText['description']; ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


<!--Call TO Action One-->
<section class="call-to-action-one">

    <!--Floted Image Left-->
    <figure class="floated-image-left wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="images/resource/man-with-food-2.png" alt=""></figure>

    <!--Floted Image Right-->
    <figure class="floated-image-right wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="images/resource/food-image-12.png" alt=""></figure>

    <div class="auto-container">
        <div class="content-box">
            <div class="sub-title">HOTLINE</div>
            <div class="phone-number">(+01)- 123 789 456</div>
            <div class="text">Trains women to work in professional kitchens, provides healthy meals for childcare centers, and uses food ...</div>
        </div>
    </div>
</section>

<?php include 'inc/footer.php'; ?>    
