<?php
include 'inc/header.php';
include_once './model/slider.php';
include_once './model/TeamMemberClass.php';

$sliderObject = new slider();
$TextObject = new TeamMemberClass();

$showAllSliderImage = $sliderObject->showSliderImages();
$showAllFeaturedImage = $sliderObject->showFeaturedProduct();
$welcomeText = $TextObject->showWelcomeText();
?>

<!--
--------------------------------------
Main Slider Start
--------------------------------------
1. Revolution Slider.

-->
<section class="main-slider" id="scroll-section-one" data-start-height="900" data-slide-overlay="yes">

    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <?php
                foreach ($showAllSliderImage as $sliderData) {
                    ?>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="img/all-images/<?php echo isset($sliderData['img_link']) ? $sliderData['img_link'] : null ?>"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="img/all-images/<?php echo isset($sliderData['img_link']) ? $sliderData['img_link'] : null ?>"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 

                        <div class="tp-caption sfl sfb tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="-150"
                             data-speed="1500"
                             data-start="500"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeIn"><h2>Welcome To APL </h2></div>

                        <div class="tp-caption sfr sfb tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="-70"
                             data-speed="1500"
                             data-start="500"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeIn"><div class="big-text"></div></div>

                        <div class="tp-caption sfl sfb tp-resizeme"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="0"
                             data-speed="1500"
                             data-start="500"
                             data-easing="easeOutExpo"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.3"
                             data-endspeed="1200"
                             data-endeasing="Power4.easeIn"><a href="gallery.php" class="theme-btn btn-style-one">View More</a></div>


                    </li>
<?php } ?>
                

            </ul>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section>

<!--
-------------------------------------
Welcome  Area
-------------------------------------

--> 
<!--Intro Section-->
<section class="intro-section">
    <div class="inner-part">
        <div class="auto-container">
            <div class="content-box">
                <div class="inner-box">
                    <!--Section Title-->
                    <div class="sec-title-one">
                        <h2>Welcome APL Bangladesh</h2>
                    </div>

                    <div class="row clearfix">
                        <!--Content Column-->
                        <div class="content-column col-md-12 col-sm-12 col-xs-12">
                            <div class="inner text-left wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="text"><?php echo $welcomeText['description']; ?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<!--
-----------------------------------
Gallery Section
-----------------------------------
-->
<section class="gallery-section">
    <div class="auto-container">
        <!--Section Title-->
        <div class="sec-title-one">
            <h2>Our Feature products</h2>
        </div>

        <div class="mixitup-gallery">

            <!--Filter List-->
            <div class="filter-list row clearfix">

                <?php
                foreach ($showAllFeaturedImage as $featuresData) {
                    ?>
                    <!--Default Food Item-->
                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image feature-img">
                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-1.jpg" title="Passion Fruits">
                                        <img src="img/all-images/<?php echo isset($featuresData['img_link']) ? $featuresData['img_link'] : null ?>" alt="">
                                    </a>
                                </figure>
                                <div class="lower-content">
                                    <h3><a href="gallery.php"><?php echo $featuresData['title']; ?></a></h3>
                                </div>
                            </div>
                        </div>
                    </div>
<?php } ?>
                <!--Default Food Item-->
                <!--                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                                        <div class="inner-box">
                                            <div class="image-box">
                                                <figure class="image feature-img">
                                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-2.jpg" title="Passion Fruits">
                                                        <img src="images/feature_products/feature-product-2.jpg" alt="">
                                                    </a>
                                                </figure>
                                                <div class="lower-content">
                                                        <h3><a href="gallery.php">Kiwi Fruits</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    Default Food Item
                                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                                        <div class="inner-box">
                                            <div class="image-box">
                                                <figure class="image feature-img">
                                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-3.jpg" title="Passion Fruits">
                                                        <img src="images/feature_products/feature-product-3.jpg" alt="">
                                                    </a>
                                                </figure>
                                                <div class="lower-content">
                                                        <h3><a href="gallery.php">Tomatoes</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    Default Food Item
                                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                                        <div class="inner-box">
                                            <div class="image-box">
                                                <figure class="image feature-img">
                                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-4.jpg" title="Passion Fruits">
                                                        <img src="images/feature_products/feature-product-4.jpg" alt="">
                                                    </a>
                                                </figure>
                                                <div class="lower-content">
                                                        <h3><a href="gallery.php">Ripe pea</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    Default Food Item
                                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                                        <div class="inner-box">
                                            <div class="image-box">
                                                <figure class="image feature-img">
                                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-5.jpg" title="Passion Fruits">
                                                        <img src="images/feature_products/feature-product-5.jpg" alt="">
                                                    </a>
                                                </figure>
                                                <div class="lower-content">
                                                        <h3><a href="gallery.php">Figs Sweet</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    Default Food Item
                                    <div class="col-md-4 col-sm-6 col-xs-12 default-food-item">
                                        <div class="inner-box">
                                            <div class="image-box">
                                                <figure class="image feature-img">
                                                    <a class="lightbox-image option-btn" data-fancybox-group="example-gallery" href="images/feature_products/feature-product-6.jpg" title="Passion Fruits">
                                                        <img src="images/feature_products/feature-product-6.jpg" alt="">
                                                    </a>
                                                </figure>
                                                <div class="lower-content">
                                                        <h3><a href="gallery.php">Rye Bread</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->

            </div>

            <!--Button Outer-->
            <div class="more-btn-outer text-center"><a href="gallery.php" class="theme-btn btn-style-four">View More</a></div>

        </div>

    </div>
</section>


<?php include 'inc/footer.php'; ?>