
<?php include 'inc/header.php';
include_once './model/slider.php';

$sliderObject = new slider();

$showAllCompleteProdImage = $sliderObject->showCompleteProjectImages();
?>    
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(images/background/Bagda.jpg);">
        <div class="auto-container">
            <h1>Products</h1>
        </div>
    </section>
    
    <!--Gallery Section-->
    <section class="gallery-section gallery-page">
    	<div class="auto-container">
            
            <div class="mixitup-gallery">

                <!--Filter List-->
                <div class="filter-list row clearfix">
					
                    <?php
            foreach ($showAllCompleteProdImage as $data) {
                ?>
                    <!--Default Portfolio Item -->
                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="img/all-images/<?php echo isset($data['img_link']) ? $data['img_link'] : null ?>" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="img/all-images/<?php echo isset($data['img_link']) ? $data['img_link'] : null ?>" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <!--Default Portfolio Item -->
<!--                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="images/product/product-5.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="images/product/product-5.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    Default Portfolio Item 
                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="images/product/product-3.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="images/product/product-3.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    Default Portfolio Item 
                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="images/product/product-4.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="images/product/product-4.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    Default Portfolio Item 
                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="images/product/product-5.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="images/product/product-5.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    Default Portfolio Item 
                    <div class="default-portfolio-item mix mix_all col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box product-img"><img src="images/product/product-6.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <div class="overlay-content">
                                        <a href="images/product/product-6.jpg" class="lightbox-image option-btn theme-btn" title="Image Caption Here" data-fancybox-group="fancybox"><span class="fa fa-search"></span></a>
                                        <a href="gallery.php" class="option-btn theme-btn"><span class="fa fa-link"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                </div>

            </div>
            
        </div>
    </section>
    
<?php include 'inc/footer.php'; ?>