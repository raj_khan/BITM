<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
include("header.php");
include_once '../model/TeamMemberClass.php';
include_once '../model/slider.php';
$teamMemberObjectEdit = new TeamMemberClass();
$sliderObjectEdit = new slider();

$editAllMember = $teamMemberObjectEdit->showForEditTeam();
include('headerMenu.php');
?>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 style="padding:5px" class="panel-title bg-slate">Your All About Info</h5>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Category</th> 
                            <th>Description</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($editAllMember as $editAll) { ?>
                            <tr>
                                <td>
                                    <?php echo $sliderObjectEdit->showCatName($editAll['category'])?>
                                </td>
                                <td>
                                    <?php echo isset($editAll['description']) ? $editAll['description'] : null ?>
                                </td>
                                <td class="text-center">
                                    <a href="editTeamMember.php?editTeam_id=<?php echo $editAll['id']; ?>" class="label label-primary">Edit</a>
                                <a href="deleteProduct.php?id=<?php echo $editAll['id']; ?>" class="label label-primary" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>