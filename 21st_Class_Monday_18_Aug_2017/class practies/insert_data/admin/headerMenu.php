
<body class="sidebar-xs has-detached-left">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <h4 class="text-center">Admin Panel of APL Bangladesh</h4>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <a href="../../trading_Admin/admin/index.php"></a>
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">
                <div class="panel panel-flat">

                    <div class="panel-body">
                        <ul class="breadcrumb-elements">

                            <li><a style="color:#0caeae; font-weight:600;" href="index.php"><i class="position-left"></i>Insert All Image</a></li>
                            <li><a style="color:#0caeae; font-weight:600;" href="insertAbout.php"><i class="position-left"></i>Insert Welcome & About Text</a></li>
<!--                            <li><a style="color:#0caeae; font-weight:600;" href="insertAboutImage.php"><i class="position-left"></i>Insert About</a></li>-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#0caeae; font-weight:600;">
                                   
                                    Product Report/Edit
                                    <span class="caret"></span>
                                </a>
                                
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="viewSlider.php" style="color:#0caeae; font-weight:600;"> All Images View</a></li>
                                    <li><a href="viewTeamMember.php" style="color:#0caeae; font-weight:600;">Welcome & About View</a></li>
<!--                                    <li><a href="viewAboutImage.php" style="color:#0caeae; font-weight:600;"> Director Pannel & Profile</a></li>-->
                                </ul>
                            </li>
                            <li><a style="color:#0caeae; font-weight:600;" href="changePass.php"><i class="position-left"></i> Change Password</a></li>
                            <li><a style="color:#0caeae; font-weight:600;" href="logout.php"> Logout  <i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                            

                        </ul>

                    </div>

                </div>

                <!-- Content area -->
                <div class="content">
                    <!-- Detached content -->
                    <div class="container-detached">
