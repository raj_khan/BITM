<?php
session_start();
if (isset($_SESSION["s_id"])) {
    header("location:../index.php");
}
include("../config/config.php");

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$error = NULL;
if (isset($_POST['login'])) {

    $email = test_input($_POST['userName']);
    $password = test_input($_POST['password']);

    $error = "";
    if ($email == "") {
        $error = "Enter Login email";
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = "Enter Valid email";
    } else if ($password == "") {
        $error = "Enter Your Password";
    }

    if ($error == "") {

        $checkQuery = "SELECT * FROM admin where email='$email' and pass ='$password' and status='1'";

        $login_check_query = mysqli_query($con, $checkQuery);

        $login_check = mysqli_fetch_array($login_check_query, MYSQLI_ASSOC);

        if (strcmp($login_check['pass'], $password) == 0) {

            $_SESSION['s_id'] = $login_check['id'];
            header("location:../index.php");
        } else {
            $error = "Username or Password Does not match";
        }
    }
}
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <title>APL BANGLADESH | Admin</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

        <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>

        <script type="text/javascript" src="../assets/js/core/app.js"></script>
        <!--/theme JS files--> 

    </head>
    <body>

        <!-- Page container -->
        <div class="page-container login-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content" >
                        <?php if (isset($error) && !empty($error)) { ?>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <div class="alert alert-warning alert-bordered">
                                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                            <span class="text-semibold"> <?php echo $error ?> </span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        <?php } ?>

                        <!-- Simple login form -->
                        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"> 
                            <div class="panel panel-body login-form">
                                <div class="text-center">

                                    <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="email" name="userName" class="form-control" placeholder="Enter Email">
                                    <div class="form-control-feedback">

                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password"  name="password" class="form-control" placeholder="Enter Password">
                                    <div class="form-control-feedback">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary btn-block" name="login" value="Login">
                                </div>

                                <!--							<div class="text-center">
                                                                                                <a href="login_password_recover.html">Forgot password?</a>
                                                                                        </div>-->
                            </div>
                        </form>
                        <!-- /simple login form -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <p class="text-center">&copy; 2017. APL- All Right Reserved. Designed by <a href="http://bsdbd.com/" target="_blank">BSD</a></p>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>