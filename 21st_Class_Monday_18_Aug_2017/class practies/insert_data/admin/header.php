<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="" type="image/x-icon">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <title>APL Admin Pannel</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/select2.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
        <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="assets/icomoon/styles.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
		
        <script type="text/javascript" src="assets/js/select2.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/core/app.js"></script>
      
        <!-- /theme JS files -->
        
   
        
        <!--Editor js file-->
        <link href="assets/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="assets/js/wysihtml5/wysihtml5.min.js"></script>
        <script type="text/javascript" src="assets/js/wysihtml5/toolbar.js"></script>
        <script type="text/javascript" src="assets/js/wysihtml5/parsers.js"></script>
        <script type="text/javascript" src="assets/js/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
        <script type="text/javascript" src="assets/js/wysihtml5/editor_wysihtml5.js"></script>
        <script type="text/javascript" src="assets/js/wysihtml5/parsers.js"></script>
        <!--/Editor js file-->

    </head>