<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}
/*========= Include All Class ========== */
include("../model/NoticeClass.php");


/*========= Declear Object ========== */
$notice = new NoticeClass();


include("header.php");
include('headerMenu.php');

if (isset($_POST['prod_upload'])) {
    $error = $notice->insert($_POST);
    }

?>
<?php if (isset($error) && !empty($error)) { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"> <?php echo $error ?> </span>
                </div>
            </div>
        </div>
    </div> 
<?php } ?>

<!-- Grid -->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- Horizontal form -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 style="padding:0 5px;"class="bg-grey-700 panel-title">Please insert About info</h5>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" method="POST"  enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-lg-2">About Text Here </label>
                        <div class="col-lg-10">
                            <textarea cols="10" rows="10" name = "description" class="wysihtml5 wysihtml5-default form-control" placeholder="Enter text ..."></textarea>
                        </div>
                    </div>

                  
                    <div class="text-right">
                        <input type="submit" name="prod_upload" value="Save"class="btn bg-grey">

                    </div>
                </form>
            </div>
        </div>
        <!-- /horizotal form -->

    </div>
</div>

<?php include('footer.php'); ?>