-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2017 at 05:07 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `formdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `formtb`
--

CREATE TABLE `formtb` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `cid` int(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `mname` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `PhoneNbr` varchar(20) NOT NULL,
  `eEmail` varchar(20) NOT NULL,
  `details` varchar(255) NOT NULL,
  `upload` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formtb`
--

INSERT INTO `formtb` (`id`, `name`, `cid`, `fname`, `mname`, `address`, `PhoneNbr`, `eEmail`, `details`, `upload`) VALUES
(1, 'shanto', 1717, 'd', 'asd', 'dhaka', '01749717065', 'shanto@gmail.com', '', '20170918165454_93708_27145.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `formtb`
--
ALTER TABLE `formtb`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `formtb`
--
ALTER TABLE `formtb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
