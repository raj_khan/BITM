-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2017 at 02:40 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_seid_176112`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` int(11) NOT NULL,
  `customer_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_phone`, `customer_address`) VALUES
(1, 'Hiron Roy', 1758693625, 'Dhaka, Bangladesh'),
(2, 'Hiron Roy', 1758693625, 'Dhaka, Bangladesh'),
(3, 'Prince', 1689369586, 'Dhanmondi, Dhaka, bangladesh'),
(4, 'Sharif', 1689322226, 'Dhanmondi, Dhaka, bangladesh'),
(5, 'Roy', 1689555560, 'Dhanmondi, Dhaka, bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_category_id` int(11) NOT NULL,
  `product_category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_category_id`, `product_category_name`) VALUES
(1, 'Laptop'),
(2, 'Desktop'),
(3, 'Bike');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `poduct_details_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `suplier_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_description` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `per_product_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`poduct_details_id`, `product_category_id`, `suplier_id`, `product_title`, `product_description`, `quantity`, `per_product_price`, `total_price`) VALUES
(1, 1, 1, 'HP Pavilion', 'HP Pavilion 14 AB022TU', '10 pcs', 10000, 10000000),
(2, 2, 2, 'Microsoft_Desktop', 'This is Good Product for us', '5 pcs', 500, 2500),
(3, 2, 3, 'Microsoft', 'This is Good Product for us', '2 pcs', 500, 1000),
(4, 3, 4, 'Bike', 'This is Good Bike for us', '2 pcs', 70000, 140000),
(5, 1, 1, 'Dell', 'Dell Description', '15 pcs', 15000, 150000);

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `suplier_id` int(11) NOT NULL,
  `suplier_name` varchar(255) NOT NULL,
  `suplier_product_name` varchar(255) NOT NULL,
  `suplier_contact_number` int(11) NOT NULL,
  `suplier_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`suplier_id`, `suplier_name`, `suplier_product_name`, `suplier_contact_number`, `suplier_address`) VALUES
(1, 'Kokhon Gharami', 'Laptop', 1958693625, 'Mirpur, Dhaka, Bangladesh'),
(2, 'Hiron Roy', 'Desktop', 1856987858, 'Dhaka, bangladesh'),
(3, 'Prince Vai', 'Bike', 1856987858, 'Dhaka, bangladesh'),
(4, 'Tomal', 'Laptop', 1856987858, 'Dhaka, bangladesh'),
(5, 'Ripon', 'Desktop', 1856987858, 'Dhaka, bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `tranjection`
--

CREATE TABLE `tranjection` (
  `tranjection_id` int(11) NOT NULL,
  `poduct_details_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `suplier_id` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `due_amount` int(11) NOT NULL,
  `status` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tranjection`
--

INSERT INTO `tranjection` (`tranjection_id`, `poduct_details_id`, `customer_id`, `suplier_id`, `total_amount`, `due_amount`, `status`) VALUES
(1, 1, 1, 1, 150000, 100000, 'complete'),
(2, 2, 2, 2, 15000, 1000, 'incomplete'),
(3, 3, 3, 3, 18000, 1800, 'complete'),
(4, 4, 4, 4, 20000, 20000, 'incomplete'),
(5, 5, 5, 5, 25000, 2000, 'complete');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`product_category_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`poduct_details_id`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`suplier_id`);

--
-- Indexes for table `tranjection`
--
ALTER TABLE `tranjection`
  ADD PRIMARY KEY (`tranjection_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `product_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `poduct_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `suplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tranjection`
--
ALTER TABLE `tranjection`
  MODIFY `tranjection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
