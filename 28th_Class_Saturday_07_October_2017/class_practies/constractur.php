<?php 
    class MyClass {
        public $prop1 = "My Name is Khan";
    
        
//        Construct ekta magic method
        public function __construct() {
    echo 'My Name is Raj';
        
}
public function setProperty($newval) {
    $this->prop1 = $newval;
}
public function getProperty() {
    return $this->prop1 . "<br />";
}
}

//Create a New object
$obj = new MyClass;

//Get This Val of $prop1
echo $obj->getProperty();

//Output a Message at the end of the file
echo "End of The Line";

?>