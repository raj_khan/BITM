<?php

class MyClass {

    public $name;
    public $age;

//        Construct ekta magic method
    public function __construct($ami, $tumi) {
        $this->name = $ami;
        $this->age = $tumi;
    }

    public function output() {
        echo $this->name;
        echo '<br />';
        echo $this->age;
    }

    }



//Create a New object
$obj = new MyClass('tomal', '30');

$obj->output();
?>