<?php

class MyClass {

    public $prop1 = "My Name is Khan";

    public function __construct() {
        echo 'This Class "', __CLASS__, '" Was Initiated!<br/>';
    }

    public function __destruct() {
        echo 'This Class "', __CLASS__, '" Was destroyed!<br/>';
    }


    public function setProperty() {
        return $this->prop1 = $newval;
    }

    public function getProperty() {
        return $this->prop1 . "<br />";
    }

}

//Create a New object
$obj = new MyClass;

//Get This Val of $prop1
echo $obj->getProperty();

//Destroye the object
unset($obj);

//Output a message at the end of line
echo 'End Of Line. <br />';
?>