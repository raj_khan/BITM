-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2016 at 10:38 AM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zsoftbd_sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_info`
--

CREATE TABLE IF NOT EXISTS `bank_info` (
  `bank_id` int(100) NOT NULL AUTO_INCREMENT,
  `b_name` varchar(100) NOT NULL,
  `b_accont_no` varchar(100) NOT NULL,
  `b_accont_type` varchar(100) NOT NULL,
  `b_address` varchar(100) NOT NULL,
  `b_phone` varchar(100) NOT NULL,
  `b_mob` varchar(100) NOT NULL,
  `b_email` varchar(100) NOT NULL,
  `b_dist` varchar(100) NOT NULL,
  `b_dis` varchar(100) NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `bank_info`
--


-- --------------------------------------------------------

--
-- Table structure for table `carttemp`
--

CREATE TABLE IF NOT EXISTS `carttemp` (
  `carttemp_hidden` int(10) NOT NULL AUTO_INCREMENT,
  `carttemp_sess` char(50) NOT NULL,
  `carttemp_item_id` char(5) NOT NULL,
  `carttemp_quan` decimal(6,2) NOT NULL,
  `carttemp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`carttemp_hidden`),
  KEY `carttemp_sess` (`carttemp_sess`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=481 ;

--
-- Dumping data for table `carttemp`
--


-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `c_id` int(100) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(100) NOT NULL,
  `c_add` varchar(100) NOT NULL,
  `c_phone` int(100) NOT NULL,
  `c_mob` int(100) NOT NULL,
  `c_email` varchar(100) NOT NULL,
  `c_dis` varchar(100) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`c_id`, `c_name`, `c_add`, `c_phone`, `c_mob`, `c_email`, `c_dis`) VALUES
(11, 'MD. HASAN ', 'PABNA ', 1727002781, 1727002781, 'tomal@gmail.com', 'pabna');

-- --------------------------------------------------------

--
-- Table structure for table `datetime`
--

CREATE TABLE IF NOT EXISTS `datetime` (
  `did` int(100) NOT NULL,
  `date` date NOT NULL,
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datetime`
--


-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `e_id` int(100) NOT NULL AUTO_INCREMENT,
  `e_name` varchar(100) NOT NULL,
  `e_add` varchar(100) NOT NULL,
  `e_phone` int(100) NOT NULL,
  `e_mob` int(100) NOT NULL,
  `e_email` varchar(100) NOT NULL,
  `e_dis` varchar(100) NOT NULL,
  `e_post` varchar(100) NOT NULL,
  `e_sal` int(100) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`e_id`, `e_name`, `e_add`, `e_phone`, `e_mob`, `e_email`, `e_dis`, `e_post`, `e_sal`) VALUES
(4, 'Mr . joy', 'gopalpur', 172000000, 172000000, 'joy@yahoo.com', 'pabna', 'pabna', 6000),
(5, 'Mr . sofiqul', 'rajapur', 172000000, 172000000, 'demo@yahoo.com', 'pabna', 'pabna', 6000),
(6, 'Mr . ahsan', 'gopalpur', 172000000, 172000000, 'ahasan@yahoo.com', 'pabna', 'pabna', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `i_id` int(100) NOT NULL AUTO_INCREMENT,
  `i_suplier` varchar(100) NOT NULL,
  `i_name` varchar(100) NOT NULL,
  `i_des` varchar(300) NOT NULL,
  `barcode` varchar(100) NOT NULL,
  `i_qun` decimal(10,2) NOT NULL,
  `i_delivery_qun` varchar(100) NOT NULL,
  `i_due_qun` varchar(100) NOT NULL,
  `i_per_price` decimal(10,2) NOT NULL,
  `direct_order_rate` varchar(100) NOT NULL,
  `national_program_rate` varchar(100) NOT NULL,
  `spacial_program_rate` varchar(100) NOT NULL,
  `product_discount_rate` varchar(100) NOT NULL,
  `i_sal_price` decimal(10,2) NOT NULL,
  `market_retailar_price` varchar(100) NOT NULL,
  `offer_prog` varchar(100) NOT NULL,
  `offer_prog_start_date` varchar(100) NOT NULL,
  `offer_prog_end_date` varchar(100) NOT NULL,
  `i_paid` decimal(10,2) NOT NULL,
  `i_due` decimal(10,2) NOT NULL,
  `i_total_price` decimal(10,2) NOT NULL,
  `product_pic_path` varchar(100) NOT NULL,
  `i_p_date` date NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`i_id`, `i_suplier`, `i_name`, `i_des`, `barcode`, `i_qun`, `i_delivery_qun`, `i_due_qun`, `i_per_price`, `direct_order_rate`, `national_program_rate`, `spacial_program_rate`, `product_discount_rate`, `i_sal_price`, `market_retailar_price`, `offer_prog`, `offer_prog_start_date`, `offer_prog_end_date`, `i_paid`, `i_due`, `i_total_price`, `product_pic_path`, `i_p_date`) VALUES
(56, '', 'Classsic chair', 'sdfsdf', '479879879', 100.00, '50', '50', 250.00, '', '3', '2', '44', 220.00, '330', 'jhgjhg', '2016-09-08', '2016-09-02', 25000.00, 1056.00, 25000.00, '', '2016-09-22'),
(52, '18', 'Classsic tabel 4', 'Classsic tabel 4', '546y5498', 100.00, '50', '50', 250.00, '', '3', '2', '10', 220.00, '330', 'ami nai nai nai ra', '2016-01-20', '2016-02-20', 25000.00, 0.00, 25000.00, 'upload/57e453d3300b5Table-9.png', '2016-09-22');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `s_id` int(100) NOT NULL AUTO_INCREMENT,
  `carttemp_sess` char(100) NOT NULL,
  `s_total` decimal(10,2) NOT NULL,
  `s_paid` decimal(10,2) NOT NULL,
  `s_due` decimal(10,2) NOT NULL,
  `s_customar_id` int(100) NOT NULL,
  `sales_details_id` int(100) NOT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=355 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`s_id`, `carttemp_sess`, `s_total`, `s_paid`, `s_due`, `s_customar_id`, `sales_details_id`) VALUES
(354, '43c6bdcfc6e6190f92936ac59362cfd0', 5200.00, 5200.00, 0.00, 11, 262),
(353, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(352, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 0, 261),
(351, '', 0.00, 0.00, 0.00, 0, 0),
(350, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(349, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(348, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(347, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(346, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(345, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(344, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(343, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(342, '805fa6e826474bbb8720950510075e85', 15600.00, 15600.00, 0.00, 11, 261),
(341, 'ee1eee2939005d462790e855daff6550', 15730.00, 15730.00, 0.00, 11, 260),
(340, 'd48275161f683e564e60b1a842ea6348', 15260.00, 15260.00, 0.00, 0, 256);

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE IF NOT EXISTS `sales_details` (
  `s_d_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_d_qun` decimal(6,2) NOT NULL,
  `s_d_i_name` varchar(100) NOT NULL,
  `s_d_price_each` decimal(10,2) NOT NULL,
  `s_d_extend_price` decimal(10,2) NOT NULL,
  `s_d_i_id` int(100) NOT NULL,
  `carttemp_sess` varchar(100) NOT NULL,
  `s_carttemp_hidden` int(100) NOT NULL,
  `s_d_cust_id` int(100) NOT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`s_d_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=263 ;

--
-- Dumping data for table `sales_details`
--

INSERT INTO `sales_details` (`s_d_id`, `s_d_qun`, `s_d_i_name`, `s_d_price_each`, `s_d_extend_price`, `s_d_i_id`, `carttemp_sess`, `s_carttemp_hidden`, `s_d_cust_id`, `sales_date`) VALUES
(262, 1.00, 'Almari big', 5200.00, 5200.00, 37, '43c6bdcfc6e6190f92936ac59362cfd0', 480, 11, '2016-09-17 20:31:57'),
(261, 3.00, 'Almari big', 5200.00, 15600.00, 37, '805fa6e826474bbb8720950510075e85', 479, 11, '2016-09-17 20:13:12'),
(260, 1.00, 'Vacuam Container ', 130.00, 130.00, 39, 'ee1eee2939005d462790e855daff6550', 478, 11, '2016-09-17 19:04:28'),
(259, 3.00, 'Almari big', 5200.00, 15600.00, 37, 'ee1eee2939005d462790e855daff6550', 477, 11, '2016-09-17 19:04:28'),
(256, 2.00, 'Vacuam Container ', 130.00, 260.00, 39, 'd48275161f683e564e60b1a842ea6348', 473, 0, '0000-00-00 00:00:00'),
(255, 2.00, 'Vacuam Container ', 130.00, 260.00, 39, 'd48275161f683e564e60b1a842ea6348', 473, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE IF NOT EXISTS `suplier` (
  `s_id` int(100) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(100) NOT NULL,
  `c_name` varchar(100) NOT NULL,
  `s_i_code` varchar(100) NOT NULL,
  `s_add` varchar(100) NOT NULL,
  `s_phone` int(100) NOT NULL,
  `s_mob` int(100) NOT NULL,
  `s_email` varchar(100) NOT NULL,
  `s_web` varchar(100) NOT NULL,
  `s_dis` varchar(100) NOT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`s_id`, `s_name`, `c_name`, `s_i_code`, `s_add`, `s_phone`, `s_mob`, `s_email`, `s_web`, `s_dis`) VALUES
(20, 'Hasan Mahadi', 'Zsoft 12', '434324234', 'adabor', 1727002781, 1727002781, 'fgdsg@gmail.com', 'sdfkbsadkjh', 'lkjlkjlk'),
(18, 'mr. tomal', 'Zsoft IT Solution Pvt 02', '12312341243124', 'adabor mohammad pur', 1727002781, 1727002781, 'sfs@yahoo.com', 'www.zsoft-bd.com', 'Dhaka'),
(21, 'Hasan Mahadi', 'Zsoft IT Solution Pvt 01', '12312341243124', 'adabor', 1727002781, 1727002781, 'fgdsg@gmail.com', 'www.zsoft.com.bd', 'Dhaka'),
(19, 'Hasan Mahadi', 'Zsoft 13', '12312341243124', 'rtjyhuiutrhy', 1727002781, 1727002781, '3456346@dsvfsd.com', 'www.zsoft.com.bd', 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `temp_order`
--

CREATE TABLE IF NOT EXISTS `temp_order` (
  `t_id` int(100) NOT NULL AUTO_INCREMENT,
  `t_i_name` varchar(100) NOT NULL,
  `t_i_qun` decimal(6,2) NOT NULL,
  `t_i_per_price` decimal(6,2) NOT NULL,
  `t_i_sal_parice` decimal(6,2) NOT NULL,
  `t_i_id` int(100) NOT NULL,
  PRIMARY KEY (`t_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=152 ;

--
-- Dumping data for table `temp_order`
--

INSERT INTO `temp_order` (`t_id`, `t_i_name`, `t_i_qun`, `t_i_per_price`, `t_i_sal_parice`, `t_i_id`) VALUES
(151, 'Almari big', 3.00, 4600.00, 5200.00, 37);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(100) NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `Username`, `Password`) VALUES
(5, 'demo', 'demo');
