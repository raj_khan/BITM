<?php  
 //test_class.php  
 include 'database.php';  
 $data = new Databases;  
 $success_message = '';  
 if(isset($_POST["submit"]))  
 {  
      $insert_data = array(  
           'post_title' =>     mysqli_real_escape_string($data->con, $_POST['post_title']),  
           'post_desc' =>      mysqli_real_escape_string($data->con, $_POST['post_desc'])  
      );  
      if($data->insert('tbl_posts', $insert_data))  
      {  
           $success_message = 'Post Inserted';  
      }       
 }  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Insert data into Table using OOPS in PHP</title>  
            
      </head>  
      <body>  
           <br /><br />  
           <div style="width:700px;">  
                <form method="post">  
                     <label>Post Title</label>  
                     <input type="text" name="post_title"/>  
                     <br />  
                     <label>Post Description</label>  
                     <textarea name="post_desc"></textarea>  
                     <br />  
                     <input type="submit" name="submit" value="Submit" />  
                     <span>  
                     <?php  
                     if(isset($success_message))  
                     {  
                          echo $success_message;  
                     }  
                     ?>  
                     </span>  
                </form>  
           </div>  
      </body>  
 </html>  
