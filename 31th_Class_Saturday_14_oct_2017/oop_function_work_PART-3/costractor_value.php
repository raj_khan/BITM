<?php
 
class MyClass
{
  public $name;
  public $age;
 
 //__construct akta magic method.
  public function __construct($ami , $tume)
  {
	  $this->name = $ami;
	  $this->age = $tume;
      
  }
 
  public function output()
  {
     echo  $this->name;
	 echo  '</br>';
	 echo  $this->age;
  }
 
  
}
 
// Create a new object
$obj = new MyClass('tomal','30');
 
$obj->output();
 
?>